<?php defined('BASEPATH') OR exit('No direct script access allowed');

class databaseSpinosum extends CI_Controller {
	protected $form;
	protected $__id;

	public function __construct(){
		parent::__construct();
		$this->__id = isset($_SESSION['id'])?($_SESSION['id']):('');
		$this->load->model('profileSpinosum_model');
		$this->load->model('log_model');
		$this->form = array(
					'profil_jenis' => array(
								'column_name' => 'profilType',
								'type' => 'select',
								'label' => 'Jenis',
								'attr' => array(
									'name' => 'profil_jenis',
									'id' => 'profil_jenis',
									'class' => 'form-control',
									'value' => '',
									'required' => 'required',
								),
								'options' => array(
									'' => '-',
									'1' => 'KOLEKTOR',
									'2' => 'PETANI',
									'3' => 'PEDAGANG BESAR',
									'4' => 'BUYER',
									'5' => 'FASILITATOR',
								),
								'selected' => ''
					),
					'profil_nama' => array(
								'column_name' => 'profilNama',
								'type' => 'input',
								'label' => 'Nama Lengkap',
								'attr' => array(
									'name' => 'profil_nama',
									'id' => 'profil_nama',
									'class' => 'form-control',
									'value' => '',
									'required' => 'required',
								)
					),
					'profil_Alamat' => array(
								'column_name' => 'profilAlamat',
								'type' => 'input',
								'label' => 'Alamat',
								'attr' => array(
									'name' => 'profil_Alamat',
									'id' => 'profil_Alamat',
									'class' => 'form-control',
									'value' => '',
									'required' => 'required',
								)
					),
					'profil_Email' => array(
								'column_name' => 'profilEmail',
								'type' => 'input',
								'label' => 'email',
								'attr' => array(
									'name' => 'profil_Email',
									'id' => 'profil_Email',
									'class' => 'form-control',
									'value' => '',
									'required' => 'required',
								)
					),
					'profil_komoditas' => array(
								'column_name' => 'profilKomoditas',
								'type' => 'select',
								'label' => 'Komoditas',
								'attr' => array(
									'name' => 'profil_komoditas',
									'id' => 'profil_komoditas',
									'class' => 'form-control',
									'value' => '',
									'required' => 'required',
								),
								'options' => array(
									'' => '-',
									'kakao' => 'KAKAO',
									'kopi' => 'KOPI',
									'rumput_laut' => 'RUMPUT LAUT',
								),
								'selected' => 'kopi'
					),
					'profil_Kelompok' => array(
								'column_name' => 'profilKelompok',
								'type' => 'input',
								'label' => 'Kelompok Tani',
								'attr' => array(
									'name' => 'profil_Kelompok',
									'id' => 'profil_Kelompok',
									'class' => 'form-control ',
									'value' => '',
								)
					),
					'profil_Provinsi' => array(
								'column_name' => 'profilProvinsi',
								'type' => 'input',
								'label' => 'Provinsi',
								'attr' => array(
									'name' => 'profil_Provinsi',
									'id' => 'profil_Provinsi',
									'class' => 'form-control ',
									'value' => '',
								),
					),
					'profil_Kabupaten' => array(
								'column_name' => 'profilKabupaten',
								'type' => 'input',
								'label' => 'Kabupaten',
								'attr' => array(
									'name' => 'profil_Kabupaten',
									'id' => 'profil_Kabupaten',
									'class' => 'form-control ',
									'value' => '',
								),
					),
					'profil_Kecamatan' => array(
								'column_name' => 'profilKecamatan',
								'type' => 'input',
								'label' => 'Kecamatan',
								'attr' => array(
									'name' => 'profil_Kecamatan',
									'id' => 'profil_Kecamatan',
									'class' => 'form-control ',
									'value' => '',
								)
					),
					'profil_Desa' => array(
								'column_name' => 'profilDesa',
								'type' => 'input',
								'label' => 'Desa',
								'attr' => array(
									'name' => 'profil_Desa',
									'id' => 'profil_Desa',
									'class' => 'form-control ',
									'value' => '',
								)
					),
					'profil_JmlKel' => array(
								'column_name' => 'profilJmlAnggotaKeluarga',
								'type' => 'input',
								'label' => 'Jumlah Anggota Keluarga',
								'attr' => array(
									'name' => 'profil_JmlKel',
									'id' => 'profil_JmlKel',
									'class' => 'form-control numOnly',
									'value' => '',
								)
					),
					'profil_sertifikat' => array(
								'column_name' => 'profilHasCert',
								'type' => 'select',
								'label' => 'Bersertifikat',
								'attr' => array(
									'name' => 'profil_sertifikat',
									'id' => 'profil_sertifikat',
									'class' => 'form-control',
									'value' => '',
								),
								'options' => array(
									'' => '-',
									'0' => 'TIDAK',
									'1' => 'IYA',
								),
								'selected' => ''
					),
					'profil_organik' => array(
								'column_name' => 'profilIsOrganic',
								'type' => 'select',
								'label' => 'Organik',
								'attr' => array(
									'name' => 'profil_organik',
									'id' => 'profil_organik',
									'class' => 'form-control',
									'value' => '',
								),
								'options' => array(
									'' => '-',
									'0' => 'TIDAK',
									'1' => 'IYA',
								),
								'selected' => ''
					),
					'profil_thnDepan' => array(
								'column_name' => 'profilPrediksiTahunDepan',
								'type' => 'input',
								'label' => 'Prediksi Tahun Depan (Ton)',
								'attr' => array(
									'name' => 'profil_thnDepan',
									'id' => 'profil_thnDepan',
									'class' => 'form-control numOnly',
									'value' => '',
								)
					),
					'profil_thnlalu' => array(
								'column_name' => 'profilPrediksiTahunLalu',
								'type' => 'input',
								'label' => 'Hasil Setahun Lalu (Ton)',
								'attr' => array(
									'name' => 'profil_thnlalu',
									'id' => 'profil_thnlalu',
									'class' => 'form-control numOnly',
									'value' => '',
								)
					),
					'user_id' => array(
								'column_name' => 'profilUserId',
								'type' => 'data',
								'attr' => array(
									'name' => 'user_id',
									'value' => (isset($this->__id)?($this->__id):('')),
									)
					),
				);

		$this->form_kebun = array(
					'user_id' => array(
								'column_name' => 'kebunUserId',
								'type' => 'data',
								'attr' => array(
									'name' => 'user_id',
									'value' => (isset($this->__id)?($this->__id):('')),
									)
					),
					'kebun_ukuran' => array(
								'column_name' => 'kebunUkuran',
								'type' => 'input',
								'label' => 'Ukuran Kebun (Ha)',
								'attr' => array(
									'name' => 'kebun_ukuran',
									'id' => 'kebun_ukuran',
									'class' => 'form-control',
									'value' => '',
									'required' => 'required',
								),
					),
					'kebun_koordinat' => array(
								'column_name' => 'kebunKoordinat',
								'type' => 'input',
								'label' => 'Koordinat GPS Kebun',
								'attr' => array(
									'name' => 'kebun_koordinat',
									'id' => 'kebun_koordinat',
									'class' => 'form-control',
									'value' => '',
								),
					),
					'kebun_tersertifikasi' => array(
								'column_name' => 'kebunSertifikasi',
								'type' => 'select',
								'label' => 'Tersertifikasi?',
								'attr' => array(
									'name' => 'kebun_tersertifikasi',
									'id' => 'kebun_tersertifikasi',
									'class' => 'form-control',
									'required' => 'required',
									'value' => '',
								),
								'options' => array(
									'' => '-',
									'YA' => 'Ya',
									'TIDAK' => 'Tidak',
								),
								'selected' => '',
					),
				);

		$this->form_volume = array(
					'user_id' => array(
								'column_name' => 'volumeUserId',
								'type' => 'data',
								'attr' => array(
									'name' => 'user_id',
									'value' => (isset($this->__id)?($this->__id):('')),
									)
					),
					'volume_timestamp' => array(
								'column_name' => 'volumeTimestamp',
								'type' => 'data',
								'attr' => array(
									'name' => 'volume_timestamp',
									'value' => time(),
									)
					),
					'volume_waste' => array(
								'column_name' => 'volumeWaste',
								'type' => 'select',
								'label' => 'Kotoran (%)',
								'attr' => array(
									'name' => 'volume_waste',
									'id' => 'volume_waste',
									'class' => 'form-control',
									'required' => 'required',
									'value' => '',
								),
								'options' => array(
									'' => '-',
									'2' => '2%',
									'3' => '3%',
									'4' => '4%',
									'5' => '5%',
								),
								'selected' => '',
					),
					'volume_kadarAir' => array(
								'column_name' => 'volumeKadarAir',
								'type' => 'select',
								'label' => 'Kadar Air (%)',
								'attr' => array(
									'name' => 'volume_kadarAir',
									'id' => 'volume_kadarAir',
									'class' => 'form-control',
									'required' => 'required',
									'value' => '',
								),
								'options' => array(
									'' => '-',
									'35%-36%' => '35%-36%',
									'37%-38%' => '37%-38%',
									'39%-40%' => '39%-40%',
								),
								'selected' => '',
					),
					'volume_beanCount' => array(
								'column_name' => 'volumeBeanCount',
								'type' => 'select',
								'label' => 'Warna',
								'attr' => array(
									'name' => 'volume_beanCount',
									'id' => 'volume_beanCount',
									'class' => 'form-control',
									'required' => 'required',
									'value' => '',
								),
								'options' => array(
									'' => '-',
									'Hitam' => 'Hitam',
									'Coklat' => 'Coklat',
									'Kuning' => 'Kuning',
								),
								'selected' => '',
					),
					'volume_weight' => array(
								'column_name' => 'volumeWeight',
								'type' => 'input',
								'label' => 'Volume (ton)',
								'attr' => array(
									'name' => 'volume_weight',
									'id' => 'volume_weight',
									'class' => 'form-control',
									'required' => 'required',
									'value' => '',
								),
					),
				);
	}

	public function index(){
		$data = null;
		$this->load->view('templates/header', $data);
		$this->load->view('templates/sidebar', $data);
		$this->load->view('templates/topbar', $data);

		$this->load->view('profileSpinosum/index.php',$data);

		$this->load->view('templates/footer',$data);
	}

	public function biodata(){
		if(!isset($_SESSION['alert'])){
			update_value_array($this->form,'profil_jenis->attr->disabled','disabled');
		}

		if(isset($_SESSION['type']) && $_SESSION['type']==3){
			update_value_array($this->form,'profil_jenis->options',array('3'=>'PEMBORONG'));
			update_value_array($this->form,'profil_jenis->selected','3');
		}

		$this->profileSpinosum_model->set_form($this->form);
		$data = $this->profileSpinosum_model->get_profile($this->__id);

		$data['title'] = 'Telusur';
		$data['form'] = $this->profileSpinosum_model->get_form();

		$this->load->view('templates/header', $data);
		$this->load->view('templates/sidebar', $data);
		$this->load->view('templates/topbar', $data);

		$this->load->view('profileSpinosum/view_profile.php',$data);
		$this->load->view('profileSpinosum/profile_js.php',$data);

		$this->load->view('templates/footer',$data);
	}

	public function dataKebun(){
		$this->profileSpinosum_model->set_form($this->form_kebun);
		$data['form_kebun'] = $this->profileSpinosum_model->get_form();
		$data['list_kebun'] = $this->profileSpinosum_model->listKebun($this->__id);
		$data['title'] = 'Data Kebun';

		$this->load->view('templates/header', $data);
		$this->load->view('templates/sidebar', $data);
		$this->load->view('templates/topbar', $data);

		$this->load->view('profileSpinosum/list_kebun.php',$data);
		$this->load->view('profileSpinosum/profile_js.php',$data);

		$this->load->view('templates/footer',$data);
	}

	public function dataVolumeSpinosum(){
		$this->profileSpinosum_model->set_form($this->form_volume);
		$data['form'] = $this->profileSpinosum_model->get_form();
		$data['list'] = $this->profileSpinosum_model->listVolume($this->__id);
		$data['title'] = 'Data Volume Spinosum yang Dimiliki';

		$this->load->view('templates/header', $data);
		$this->load->view('templates/sidebar', $data);
		$this->load->view('templates/topbar', $data);

		$this->load->view('profileSpinosum/data_volume.php',$data);
		$this->load->view('profileSpinosum/profile_js.php',$data);

		$this->load->view('templates/footer',$data);
	}

	public function save_profile(){
		if(empty($_POST)){
			redirect(site_url('databaseSpinosum/biodata'));
			return;
		}

		$this->profileSpinosum_model->save_profile($this->__id,$this->form,$_POST);
		redirect(site_url('databaseSpinosum/biodata'));
	}

	public function save_kebun(){
		if(empty($_POST)){
			redirect(site_url('databaseSpinosum/dataKebun'));
			return;
		}

		$this->profileSpinosum_model->save_kebun($this->__id,$this->form_kebun,$_POST);
		redirect(site_url('databaseSpinosum/dataKebun'));
	}

	public function save_volume(){
		if(empty($_POST)){
			redirect(site_url('databaseSpinosum/dataVolumeSpinosum'));
			return;
		}

		$this->profileSpinosum_model->save_volume($this->__id,$this->form_volume,$_POST);
		redirect(site_url('databaseSpinosum/dataVolumeSpinosum'));
	}

	public function getRegencies(){
		if(isset($_POST['province'])){
			echo json_encode($this->profileSpinosum_model->getRegencies($_POST['province']));
		}
	}

	public function delete_kebun($kebunId){
		$this->profileSpinosum_model->delete_kebun($kebunId);
		redirect(site_url('databaseSpinosum/dataKebun'));
	}

	public function delete_log($logId){
		$this->log_model->delete_log($logId);
		redirect(site_url('log'));
	}
}
?>
