<?php defined('BASEPATH') OR exit('No direct script access allowed');

class infoHarga extends CI_Controller {
	protected $__id;

	public function __construct(){
		parent::__construct();
		$this->__id = isset($_SESSION['id'])?($_SESSION['id']):('');
		$this->load->model('file_model');
	}

	public function index(){
		$data['files']=$this->file_model->get_files_from_db();
		$data['price']['title'] = 'ICCO Daily Prices of Cocoa Beans';
		$data['price']['table'] = $this->get_ICCO();

		$this->load->view('templates/header', $data);
		$this->load->view('templates/sidebar', $data);
		$this->load->view('templates/topbar', $data);

		$this->load->view('dashboard/harga.php',$data);

		$this->load->view('templates/footer');
	}

	protected function get_ICCO(){
		$content = file_get_contents('https://www.icco.org/statistics/cocoa-prices/daily-prices.html');
		$index['id']  = strpos($content,'mod_statistics_latest');
		$index['tag_open'] = strpos($content,'<table',($index['id']-20));
		$index['tag_close'] = strpos($content,'</table>');

		$table = substr($content,$index['tag_open'],(($index['tag_close']+strlen('</table>'))-$index['tag_open']));

		$table = str_replace('<table ','<table class="table table-striped" ',$table);

		return $table;
	}

}
?>
