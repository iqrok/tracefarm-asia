<?php defined('BASEPATH') OR exit('No direct script access allowed');

class profile extends CI_Controller {
	protected $form;
	protected $__id;

	public function __construct(){
		parent::__construct();
		$this->__id = isset($_SESSION['id'])?($_SESSION['id']):('');
		$this->load->model('profile_model');
		$this->load->model('log_model');
		$this->form = array(
					'profil_jenis' => array(
								'column_name' => 'profilType',
								'type' => 'select',
								'label' => 'Jenis',
								'attr' => array(
									'name' => 'profil_jenis',
									'id' => 'profil_jenis',
									'class' => 'form-control',
									'value' => '',
									'required' => 'required',
								),
								'options' => array(
									'' => '-',
									'1' => 'KOLEKTOR',
									'2' => 'PETANI',
								),
								'selected' => ''
					),
					'profil_nama' => array(
								'column_name' => 'profilNama',
								'type' => 'input',
								'label' => 'Nama Lengkap',
								'attr' => array(
									'name' => 'profil_nama',
									'id' => 'profil_nama',
									'class' => 'form-control',
									'value' => '',
									'required' => 'required',
								)
					),
					'profil_Alamat' => array(
								'column_name' => 'profilAlamat',
								'type' => 'input',
								'label' => 'Alamat',
								'attr' => array(
									'name' => 'profil_Alamat',
									'id' => 'profil_Alamat',
									'class' => 'form-control',
									'value' => '',
									'required' => 'required',
								)
					),
					'profil_Email' => array(
								'column_name' => 'profilEmail',
								'type' => 'input',
								'label' => 'email',
								'attr' => array(
									'name' => 'profil_Email',
									'id' => 'profil_Email',
									'class' => 'form-control',
									'value' => '',
									'required' => 'required',
								)
					),
					'profil_komoditas' => array(
								'column_name' => 'profilKomoditas',
								'type' => 'select',
								'label' => 'Komoditas',
								'attr' => array(
									'name' => 'profil_komoditas',
									'id' => 'profil_komoditas',
									'class' => 'form-control',
									'value' => '',
									'required' => 'required',
								),
								'options' => array(
									'' => '-',
									'kopi' => 'KOPI',
									'rumput_laut' => 'RUMPUT LAUT',
								),
								'selected' => 'kopi'
					),
					'profil_Kelompok' => array(
								'column_name' => 'profilKelompok',
								'type' => 'input',
								'label' => 'Kelompok Tani',
								'attr' => array(
									'name' => 'profil_Kelompok',
									'id' => 'profil_Kelompok',
									'class' => 'form-control ',
									'value' => '',
								)
					),
					'profil_Provinsi' => array(
								'column_name' => 'profilProvinsi',
								'type' => 'input',
								'label' => 'Provinsi',
								'attr' => array(
									'name' => 'profil_Provinsi',
									'id' => 'profil_Provinsi',
									'class' => 'form-control ',
									'value' => '',
								),
					),
					'profil_Kabupaten' => array(
								'column_name' => 'profilKabupaten',
								'type' => 'input',
								'label' => 'Kabupaten',
								'attr' => array(
									'name' => 'profil_Kabupaten',
									'id' => 'profil_Kabupaten',
									'class' => 'form-control ',
									'value' => '',
								),
					),
					'profil_Kecamatan' => array(
								'column_name' => 'profilKecamatan',
								'type' => 'input',
								'label' => 'Kecamatan',
								'attr' => array(
									'name' => 'profil_Kecamatan',
									'id' => 'profil_Kecamatan',
									'class' => 'form-control ',
									'value' => '',
								)
					),
					'profil_Desa' => array(
								'column_name' => 'profilDesa',
								'type' => 'input',
								'label' => 'Desa',
								'attr' => array(
									'name' => 'profil_Desa',
									'id' => 'profil_Desa',
									'class' => 'form-control ',
									'value' => '',
								)
					),
					'profil_JmlKel' => array(
								'column_name' => 'profilJmlAnggotaKeluarga',
								'type' => 'input',
								'label' => 'Jumlah Anggota Keluarga',
								'attr' => array(
									'name' => 'profil_JmlKel',
									'id' => 'profil_JmlKel',
									'class' => 'form-control numOnly',
									'value' => '',
								)
					),
					'profil_sertifikat' => array(
								'column_name' => 'profilHasCert',
								'type' => 'select',
								'label' => 'Bersertifikat',
								'attr' => array(
									'name' => 'profil_sertifikat',
									'id' => 'profil_sertifikat',
									'class' => 'form-control',
									'value' => '',
								),
								'options' => array(
									'' => '-',
									'0' => 'TIDAK',
									'1' => 'IYA',
								),
								'selected' => ''
					),
					'profil_organik' => array(
								'column_name' => 'profilIsOrganic',
								'type' => 'select',
								'label' => 'Organik',
								'attr' => array(
									'name' => 'profil_organik',
									'id' => 'profil_organik',
									'class' => 'form-control',
									'value' => '',
								),
								'options' => array(
									'' => '-',
									'0' => 'TIDAK',
									'1' => 'IYA',
								),
								'selected' => ''
					),
					'profil_thnDepan' => array(
								'column_name' => 'profilPrediksiTahunDepan',
								'type' => 'input',
								'label' => 'Prediksi Tahun Depan (Ton)',
								'attr' => array(
									'name' => 'profil_thnDepan',
									'id' => 'profil_thnDepan',
									'class' => 'form-control numOnly',
									'value' => '',
								)
					),
					'profil_thnlalu' => array(
								'column_name' => 'profilPrediksiTahunLalu',
								'type' => 'input',
								'label' => 'Hasil Setahun Lalu (Ton)',
								'attr' => array(
									'name' => 'profil_thnlalu',
									'id' => 'profil_thnlalu',
									'class' => 'form-control numOnly',
									'value' => '',
								)
					),
					'user_id' => array(
								'column_name' => 'profilUserId',
								'type' => 'data',
								'attr' => array(
									'name' => 'user_id',
									'value' => (isset($this->__id)?($this->__id):('')),
									)
					),
				);
		
		$this->form_kebun = array(
					'user_id' => array(
								'column_name' => 'kebunUserId',
								'type' => 'data',
								'attr' => array(
									'name' => 'user_id',
									'value' => (isset($this->__id)?($this->__id):('')),
									)
					),
					'kebun_ukuran' => array(
								'column_name' => 'kebunUkuran',
								'type' => 'input',
								'label' => 'Ukuran Kebun (Ha)',
								'attr' => array(
									'name' => 'kebun_ukuran',
									'id' => 'kebun_ukuran',
									'class' => 'form-control',
									'value' => '',
									'required' => 'required',
								),
					),
					'kebun_koordinat' => array(
								'column_name' => 'kebunKoordinat',
								'type' => 'input',
								'label' => 'Koordinat GPS Kebun',
								'attr' => array(
									'name' => 'kebun_koordinat',
									'id' => 'kebun_koordinat',
									'class' => 'form-control',
									'value' => '',
								),
					),
				);
	}

	public function index(){
		if(!isset($_SESSION['alert'])){
			update_value_array($this->form,'profil_jenis->attr->disabled','disabled');
		}

		if(isset($_SESSION['type']) && $_SESSION['type']==3){
			update_value_array($this->form,'profil_jenis->options',array('3'=>'PEMBORONG'));
			update_value_array($this->form,'profil_jenis->selected','3');
		}
		
		$this->profile_model->set_form($this->form);
		$data = $this->profile_model->get_profile($this->__id);

		$data['title'] = 'Telusur';
		$data['form'] = $this->profile_model->get_form();
		
		$this->profile_model->set_form($this->form_kebun);
		$data['form_kebun'] = $this->profile_model->get_form();
		$data['list_kebun'] = $this->profile_model->listKebun($this->__id);
		

		$this->load->view('templates/header', $data);
		$this->load->view('templates/sidebar', $data);
		$this->load->view('templates/topbar', $data);

		$this->load->view('profile/view_profile.php',$data);
		$this->load->view('profile/profile_js.php',$data);

		$this->load->view('templates/footer',$data);
	}

	public function save_profile(){
		if(empty($_POST)){
			redirect(site_url('profile'));
			return;
		}

		$this->profile_model->save_profile($this->__id,$this->form,$_POST);
		redirect(site_url('profile'));
	}

	public function save_kebun(){
		if(empty($_POST)){
			redirect(site_url('profile'));
			return;
		}

		$this->profile_model->save_kebun($this->__id,$this->form_kebun,$_POST);
		redirect(site_url('profile'));
	}

	public function getRegencies(){
		if(isset($_POST['province'])){
			echo json_encode($this->profile_model->getRegencies($_POST['province']));
		}
	}

	public function getDistricts(){
		if(isset($_POST['province']) && isset($_POST['kabupaten'])){
			echo json_encode($this->profile_model->getDistricts($_POST['province'],$_POST['kabupaten']));
			//~ echo $this->profile_model->getDistricts($_POST['province'],$_POST['kabupaten']);
			//~ echo "wut";
		}
		else{
			//~ echo ":asasasa";
			echo json_encode($this->profile_model->getDistricts("SULAWESI SELATAN","PINRANG"));
		}
	}

	public function getVillages(){
		if(isset($_POST['province']) && isset($_POST['kabupaten']) && isset($_POST['kecamatan'])){
			echo json_encode($this->profile_model->getVillages($_POST['province'],$_POST['kabupaten'],$_POST['kecamatan']));
			//~ echo $this->profile_model->getDistricts($_POST['province'],$_POST['kabupaten']);
			//~ echo "wut";
		}
		else{
			//~ echo ":asasasa";
			echo json_encode($this->profile_model->getVillages("SULAWESI SELATAN","PINRANG","LEMBANG"));
		}
	}

	public function delete_kebun($kebunId){
		$this->profile_model->delete_kebun($kebunId);
		redirect(site_url('profile'));
	}

	public function delete_log($logId){
		$this->log_model->delete_log($logId);
		redirect(site_url('log'));
	}
}
?>
