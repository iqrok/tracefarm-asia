<?php defined('BASEPATH') OR exit('No direct script access allowed');

class rekapitulasi extends CI_Controller {
	protected $__id;

	public function __construct(){
		parent::__construct();
		$this->__id = isset($_SESSION['id'])?($_SESSION['id']):('');
		$this->load->model('log_model');
		$this->load->model('file_model');
		$this->load->library('blo_highcharts');

	}

	public function index(){
		$data['files']=$this->file_model->get_files_from_db();
		//$data['price']['title'] = 'ICCO Daily Prices of Cocoa Beans';
		//$data['price']['table'] = $this->get_ICCO();
		$charts = $this->get_charts();
		$data['chart'] = 'highcharts';

		$this->load->view('templates/header', $data);
		$this->load->view('templates/sidebar', $data);
		$this->load->view('templates/topbar', $data);

		$this->load->view('dashboard/rekapitulasi.php',$data);
		//$this->load->view('dashboard/harga.php',$data);

		if($charts !== false)
		    $this->load->view('log/log_charts.php',$charts);

		$this->load->view('templates/footer');
	}

	protected function get_ICCO(){
		$content = file_get_contents('https://www.icco.org/statistics/cocoa-prices/daily-prices.html');
		$index['id']  = strpos($content,'mod_statistics_latest');
		$index['tag_open'] = strpos($content,'<table',($index['id']-20));
		$index['tag_close'] = strpos($content,'</table>');

		$table = substr($content,$index['tag_open'],(($index['tag_close']+strlen('</table>'))-$index['tag_open']));

		$table = str_replace('<table ','<table class="table table-striped" ',$table);

		return $table;
	}

	protected function get_charts(){

		switch($_SESSION['type']){
		case 1:
			$type = array(
							'self' => 'Kolektor',
							'else' => 'Petani',
						);
			break;
		case 2:
			$type = array(
							'self' => 'Petani',
							'else' => 'Kolektor',
						);
			break;
		case 3:
			$type = false;
			break;
		default:
			$type = array(
							'self' => 'Kolektor',
							'else' => 'Petani',
						);
			break;
		}

		if($type==false)
			return false;
			
		$userId = $this->__id;
		$chart_config = array(
							'type' => 'column',
							'height' => '200',
							'title' => '',
							'single_series' => 'true',
							'legend' => 'false',
							'y_axis_title' => '',
							'categories' => '',
							'tooltips' => '',
							'series' => '',
							'script' => '',
							'responsive' => '
								rules: [{
									condition: {
										maxWidth: 500
									}
								}]'
						);

		//generate chart for each receiverId
		$chart_data = $this->log_model->get_log($userId,null,null,1);
		$details = $this->blo_highcharts->generate_categories_series($chart_data,'profilNama','logVolume');
		$id= 'receiver_chart';
		$this->blo_highcharts->init($id,$chart_config);
		$this->blo_highcharts->set_type($id,'column');
		$this->blo_highcharts->set_title($id,' ');
		$this->blo_highcharts->set_y_axis_title($id,'Volume (Ton)');
		$this->blo_highcharts->set_categories($id,$details['categories']);
		$this->blo_highcharts->set_series($id,$details['series'],'Gelombang');
		$this->blo_highcharts->set_use_tooltips($id,true);
		$this->blo_highcharts->generate_script($id);
		$charts['container'][$id] = $this->blo_highcharts->get_container($id);
		$charts['box_title'][$id] = 'Rekapitulasi Volume Bulan Ini berdasarkan Pembeli';

		//generate chart for each month
		$chart_data = $this->log_model->get_log($userId,false,false,2);
		$details = $this->blo_highcharts->generate_categories_series($chart_data,'MONTHYEAR','logVolume');
		$id= 'monthly_recap';
		$this->blo_highcharts->init($id,$chart_config);
		$this->blo_highcharts->set_type($id,'column');
		$this->blo_highcharts->set_title($id,' ');
		$this->blo_highcharts->set_y_axis_title($id,'Volume (Ton)');
		$this->blo_highcharts->set_categories($id,$details['categories']);
		$this->blo_highcharts->set_series($id,$details['series'],'Gelombang');
		$this->blo_highcharts->set_use_tooltips($id,true);
		$this->blo_highcharts->generate_script($id);
		$charts['container'][$id] = $this->blo_highcharts->get_container($id);
		$charts['box_title'][$id] = 'Rekapitulasi Volume Per Bulan';

		$charts['script'] = $this->blo_highcharts->get_all_scripts();

		return $charts;
	}
}
?>
