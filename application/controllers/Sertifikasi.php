<?php defined('BASEPATH') OR exit('No direct script access allowed');

class sertifikasi extends CI_Controller {
	protected $form;
	protected $__id;

	public function __construct(){
		parent::__construct();
		$this->__id = isset($_SESSION['id'])?($_SESSION['id']):('');
		$this->load->model('sertifikasi_model');
		$this->load->model('profile_model');
	}

	public function index(){

		$data['formSertifikasi'] = $this->sertifikasi_model->getFormSertifikasi($this->__id);
		$data['listUser'] = $this->sertifikasi_model->getAllPetani();
		
		$data['title'] = 'Sertifikasi';
		$data['userId'] = $this->__id;

		$this->load->view('templates/header', $data);
		$this->load->view('templates/sidebar', $data);
		$this->load->view('templates/topbar', $data);

		$this->load->view('sertifikasi/form_sertifikasi.php',$data);

		$this->load->view('templates/footer',$data);
	}

	public function listUserTersetifikasi(){		
		$data['table'] = $this->sertifikasi_model->listUserTersetifikasi();
		$data['title'] = 'List Petani Tertifikasi';

		$this->load->view('templates/header', $data);
		$this->load->view('templates/sidebar', $data);
		$this->load->view('templates/topbar', $data);

		$this->load->view('sertifikasi/list_tersertifikasi.php',$data);

		$this->load->view('templates/footer',$data);
	}

	public function hasilSertifikasi($userId){
		$data['profile'] = $this->profile_model->get_profile_raw($userId);
		$data['formSertifikasi'] = $this->sertifikasi_model->getHasilSertifikasi($userId);
		$data['nilaiSertifikasi'] = $this->sertifikasi_model->penilaian_sertifikasi($userId);
		$data['hasilSertifikasi'] = $this->sertifikasi_model->parseHasil($data['nilaiSertifikasi'])? "<span style='color:green;'>LOLOS</span>" : "<span style='color:red;'>TIDAK LOLOS</span>";;
		//~ $data['listUser'] = $this->sertifikasi_model->getAllPetani() 
		
		$data['title'] = 'Hasil Sertifikasi';
		//~ $data['userId'] = $this->__id;
        echo "<!--";var_dump($data['nilaiSertifikasi']);echo"-->";
		$this->load->view('templates/header', $data);
		$this->load->view('templates/sidebar', $data);
		$this->load->view('templates/topbar', $data);

		$this->load->view('sertifikasi/hasil_sertifikasi.php',$data);

		$this->load->view('templates/footer',$data);
	}

	public function save_form(){
		if(empty($_POST)){
			redirect(site_url('sertifikasi'));
			return;
		}
		$userId = $_POST['userId'];
		unset($_POST['userId']);

		$isExist = $this->sertifikasi_model->checkSertifikasiExist($userId);
		if(!$isExist){
			$this->sertifikasi_model->initSertifikasi($userId);
		}
		//~ var_dump($isExist);
		//~ var_dump($userId);
		//~ var_dump($_POST);
		$this->sertifikasi_model->save_form_sertifikasi($userId,$_POST);
		redirect(site_url('sertifikasi'));
	}
}
?>
