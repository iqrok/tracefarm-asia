<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class listuser extends CI_Controller {
	public function __construct(){
		parent::__construct();		
		$this->load->model('listuser_model');
		$this->typeName = array(1=>"KOLEKTOR",2=>"PETANI");
	}
	
	public function index(){
		redirect('listuser/semua/2');
		return;
	}
	
	public function semua($type=2){
		$data['table'] = $this->listuser_model->list_user($type);
		
		$this->load->view('templates/header', $data);
		$this->load->view('templates/sidebar', $data);
		$this->load->view('templates/topbar', $data);

		$this->load->view('list/view_list.php',$data);
		
		$this->load->view('templates/footer',$data);
		return;
	}
}
