<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class log extends CI_Controller {
	protected $redirect_to = 'dashboard';
	protected $__id;

	public function __construct(){
		parent::__construct();
		$this->__id = isset($_SESSION['id'])?($_SESSION['id']):('');
		$this->load->model('log_model');
		$this->load->model('profile_model');
		$this->load->library('blo_highcharts');
		$this->form = array(
					'log_receiver' => array(
								'column_name' => 'logReceiverId',
								'type' => 'select',
								'label' => '',
								'attr' => array(
									'name' => 'log_receiver',
									'id' => 'log_receiver',
									'class' => 'form-control',
									'value' => '',
									'required' => 'required',
								),
								'options' => array(
									'' => '-',
								),
								'selected' => ''
					),
					'log_volume' => array(
								'column_name' => 'logVolume',
								'type' => 'input',
								'label' => 'Volume (dalam Ton)',
								'attr' => array(
									'name' => 'log_volume',
									'id' => 'log_volume',
									'class' => 'form-control numOnly',
									'placeholder' => 'Volume (Ton)',
									'required' => 'required',
								)
					),
					'log_time' => array(
								'column_name' => 'logTimestamp',
								'type' => 'input',
								'label' => 'Waktu',
								'attr' => array(
									'name' => 'log_time',
									'id' => 'log_time',
									'class' => 'form-control datepicker',
									'placeholder' => 'DD-MM-YYYY',
									'required' => 'required',
								)
					),
					'user_id' => array(
								'column_name' => 'logUserId',
								'type' => 'data',
								'attr' => array(
									'name' => 'user_id',
									'value' => $this->__id,
									)
					),
				);
	}

	public function index(){		
		if(isset($_GET['range'])){
			$range = explode('-',$_GET['range']);
			if(isset($range[0]) && isset($range[1])){
				$month = $range[0];
				$year = $range[1];
			}
			else{
				$month = date('m',time());
				$year = date('Y',time());
			}
		}
		else{
			$month = date('m',time());
			$year = date('Y',time());
		}
		
		$userId = $this->__id;
		$profile = $this->profile_model->get_profile_raw($userId);
		$listReceiver = $this->log_model->get_receiver_list($profile['profilType']);

		$this->form['log_receiver']['label'] = $listReceiver['receiverType'];

		$this->form['log_receiver']['options'][''] = ['-'];
		foreach($listReceiver['receiverList'] as $key=>$val){
			$this->form['log_receiver']['options'][$val['RECEIVER_ID']] = $val['NAME'];
		}

		$this->log_model->set_form($this->form);
		$data['title'] = 'Log Jual/Beli';
		$data['form'] = $this->log_model->get_form();
		$data['chart'] = 'highcharts';

		$data['title_log'] = 'Log Transaksi Rantai Pasok';
		$data['log'] = $this->log_model->get_log($this->__id,$month,$year,3);
		$data['log_table'] = $this->load->view('log/log_table', $data, true);
		
		$charts = $this->get_charts();

		$this->load->view('templates/header', $data);
		$this->load->view('templates/sidebar', $data);
		$this->load->view('templates/topbar', $data);

		$this->load->view('log/log_form.php',$data);
		$this->load->view('log/log_charts.php',$charts);
		$this->load->view('log/view_table.php',$data);

		$this->load->view('templates/footer',$data);
	}

	protected function get_charts(){

		switch($_SESSION['type']){
		case 1:
			$type = array(
							'self' => 'Kolektor',
							'else' => 'Petani',
						);
			break;
		case 2:
			$type = array(
							'self' => 'Petani',
							'else' => 'Kolektor',
						);
			break;
		}

		$userId = $this->__id;
		$chart_config = array(
							'type' => 'column',
							'height' => '400',
							'title' => '',
							'single_series' => 'true',
							'legend' => 'false',
							'y_axis_title' => '',
							'categories' => '',
							'tooltips' => '',
							'series' => '',
							'script' => '',
							'responsive' => '
								rules: [{
									condition: {
										maxWidth: 500
									}
								}]'
						);

		//generate chart for each receiverId
		$chart_data = $this->log_model->get_log($userId,null,null,1);
		$details = $this->blo_highcharts->generate_categories_series($chart_data,'profilNama','logVolume');
		$id= 'receiver_chart';
		$this->blo_highcharts->init($id,$chart_config);
		$this->blo_highcharts->set_type($id,'column');
		$this->blo_highcharts->set_title($id,' ');
		$this->blo_highcharts->set_y_axis_title($id,'Volume (Ton)');
		$this->blo_highcharts->set_categories($id,$details['categories']);
		$this->blo_highcharts->set_series($id,$details['series'],'Gelombang');
		$this->blo_highcharts->set_use_tooltips($id,true);
		$this->blo_highcharts->generate_script($id);
		$charts['container'][$id] = $this->blo_highcharts->get_container($id);
		$charts['box_title'][$id] = 'Rekapitulasi Volume Penjualan dari Petani';
		//$charts['box_title'][$id] = 'Rekapitulasi Per '.$type['else'];

		//generate chart for each month
		$chart_data = $this->log_model->get_log($userId,false,false,2);
		$details = $this->blo_highcharts->generate_categories_series($chart_data,'MONTHYEAR','logVolume');
		$id= 'monthly_recap';
		$this->blo_highcharts->init($id,$chart_config);
		$this->blo_highcharts->set_type($id,'column');
		$this->blo_highcharts->set_title($id,' ');
		$this->blo_highcharts->set_y_axis_title($id,'Volume (Ton)');
		$this->blo_highcharts->set_categories($id,$details['categories']);
		$this->blo_highcharts->set_series($id,$details['series'],'Gelombang');
		$this->blo_highcharts->set_use_tooltips($id,true);
		$this->blo_highcharts->generate_script($id);
		$charts['container'][$id] = $this->blo_highcharts->get_container($id);
		$charts['box_title'][$id] = 'Rekapitulasi Per Bulan';

		$charts['script'] = $this->blo_highcharts->get_all_scripts();

		return $charts;
	}

	public function save_log(){
		if(empty($_POST)){
			redirect(site_url('log'));
			return;
		}

		$this->log_model->save_form_log($this->form,$_POST);
		redirect(site_url('log'));
	}
}
