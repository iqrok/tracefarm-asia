<?php defined('BASEPATH') OR exit('No direct script access allowed');

class infoBudidaya extends CI_Controller {
	protected $__id;

	public function __construct(){
		parent::__construct();
		$this->__id = isset($_SESSION['id'])?($_SESSION['id']):('');
		$this->load->model('file_model');
	}

	public function index(){
		$data['files']=$this->file_model->get_files_from_db();

		$this->load->view('templates/header', $data);
		$this->load->view('templates/sidebar', $data);
		$this->load->view('templates/topbar', $data);

		$this->load->view('dashboard/dashboard.php',$data);

		$this->load->view('templates/footer');
	}
}
?>
