<?php defined('BASEPATH') OR exit('No direct script access allowed');

class dataVolumeKopi extends CI_Controller {
	protected $form;
	protected $__id;

	public function __construct(){
		parent::__construct();
		$this->__id = isset($_SESSION['id'])?($_SESSION['id']):('');
		$this->load->model('profileKopi_model');
		$this->load->model('dataVolumeKopi_model');

		$this->load->library("pagination");
		$this->load->library("blo_table");
		$this->load->library("blo_search");

		$this->search_form = array(
					'profil_Provinsi' => array(
								'column_name' => 'bb.profilProvinsi',
								'type' => 'select',
								'label' => 'Provinsi',
								'attr' => array(
									'name' => 'profil_Provinsi',
									'id' => 'profil_Provinsi',
									'class' => 'form-control ',
									'required'=>'required',
									'value' => '',
								),
								'options' => $this->profileKopi_model->getProvinces(),
								'selected'=>'',
					),
					'profil_Kabupaten' => array(
								'column_name' => 'bb.profilKabupaten',
								'type' => 'select',
								'label' => 'Kabupaten',
								'attr' => array(
									'name' => 'profil_Kabupaten',
									'id' => 'profil_Kabupaten',
									'class' => 'form-control ',
									'required'=>'required',
									'value' => '',
								),
								'options' => array(),
								'selected'=>'',
					),
					'volume_waste' => array(
								'column_name' => 'aa.volumeWaste',
								'type' => 'select',
								'label' => 'Waste (%)',
								'attr' => array(
									'name' => 'volume_waste',
									'id' => 'volume_waste',
									'class' => 'form-control',
									'value' => '',
								),
								'options' => array(
									'' => '-',
									'2.5' => '2,5%',
									'3' => '3%',
									'4' => '4%',
									'5' => '5%',
									'6' => '6%',
									'7' => '7%',
									'8' => '8%',
									'9' => '9%',
									'10' => '10%',
								),
								'selected' => '',
					),
					'volume_kadarAir' => array(
								'column_name' => 'aa.volumeKadarAir',
								'type' => 'select',
								'label' => 'Kadar Air (%)',
								'attr' => array(
									'name' => 'volume_kadarAir',
									'id' => 'volume_kadarAir',
									'class' => 'form-control',
									'value' => '',
								),
								'options' => array(
									'' => '-',
									'7' => '7%',
									'8' => '8%',
									'9' => '9%',
									'10' => '10%',
								),
								'selected' => '',
					),
					'volume_beanCount' => array(
								'column_name' => 'aa.volumeBeanCount',
								'type' => 'select',
								'label' => 'Bean Count',
								'attr' => array(
									'name' => 'volume_beanCount',
									'id' => 'volume_beanCount',
									'class' => 'form-control',
									'value' => '',
								),
								'options' => array(
									'' => '-',
									'AA' => 'AA (<85 biji)',
									'A' => 'A (85-100 biji)',
									'B' => 'B (101-110 biji)',
									'C' => 'C (111-120 biji)',
									'S' => 'D (>120 biji)',
								),
								'selected' => '',
					),
					'volume_tersertifikasi' => array(
								'column_name' => 'aa.volumeTersertifikasi',
								'type' => 'select',
								'label' => 'Tersertifikasi?',
								'attr' => array(
									'name' => 'volume_beanCount',
									'id' => 'volume_beanCount',
									'class' => 'form-control',
									'value' => '',
								),
								'options' => array(
									'' => '-',
									'YA' => 'YA',
									'TIDAK' => 'TIDAK',
								),
								'selected' => '',
					),
				);

		$this->search_form_wilayah = array(
					'profil_Provinsi' => array(
								'column_name' => 'bb.profilProvinsi',
								'type' => 'select',
								'label' => 'Provinsi',
								'attr' => array(
									'name' => 'profil_Provinsi',
									'id' => 'profil_Provinsi',
									'class' => 'form-control ',
									'required'=>'required',
									'value' => '',
								),
								'options' => $this->profileKopi_model->getProvinces(),
								'selected'=>'',
					),
					'profil_Kabupaten' => array(
								'column_name' => 'bb.profilKabupaten',
								'type' => 'select',
								'label' => 'Kabupaten',
								'attr' => array(
									'name' => 'profil_Kabupaten',
									'id' => 'profil_Kabupaten',
									'class' => 'form-control ',
									'required'=>'required',
									'value' => '',
								),
								'options' => array(),
								'selected'=>'',
					),
					'profil_Kecamatan' => array(
								'column_name' => 'bb.profilKecamatan',
								'type' => 'select',
								'label' => 'Kecamatan',
								'attr' => array(
									'name' => 'profil_Kecamatan',
									'id' => 'profil_Kecamatan',
									'class' => 'form-control ',
									'required'=>'required',
									'value' => '',
								),
								'options' => array(),
								'selected'=>'',
					),
					'profil_Kelurahan' => array(
								'column_name' => 'bb.profilDesa',
								'type' => 'select',
								'label' => 'Desa',
								'attr' => array(
									'name' => 'profil_Kelurahan',
									'id' => 'profil_Kelurahan',
									'class' => 'form-control ',
									'required'=>'required',
									'value' => '',
								),
								'options' => array(),
								'selected'=>'',
					),
					'volume_waste' => array(
								'column_name' => 'aa.volumeWaste',
								'type' => 'select',
								'label' => 'Waste (%)',
								'attr' => array(
									'name' => 'volume_waste',
									'id' => 'volume_waste',
									'class' => 'form-control',
									'value' => '',
								),
								'options' => array(
									'' => '-',
									'2.5' => '2,5%',
									'3' => '3%',
									'4' => '4%',
									'5' => '5%',
									'6' => '6%',
									'7' => '7%',
									'8' => '8%',
									'9' => '9%',
									'10' => '10%',
								),
								'selected' => '',
					),
					'volume_kadarAir' => array(
								'column_name' => 'aa.volumeKadarAir',
								'type' => 'select',
								'label' => 'Kadar Air (%)',
								'attr' => array(
									'name' => 'volume_kadarAir',
									'id' => 'volume_kadarAir',
									'class' => 'form-control',
									'value' => '',
								),
								'options' => array(
									'' => '-',
									'7' => '7%',
									'8' => '8%',
									'9' => '9%',
									'10' => '10%',
								),
								'selected' => '',
					),
					'volume_beanCount' => array(
								'column_name' => 'aa.volumeBeanCount',
								'type' => 'select',
								'label' => 'Grade Defect',
								'attr' => array(
									'name' => 'volume_beanCount',
									'id' => 'volume_beanCount',
									'class' => 'form-control',
									'value' => '',
								),
								'options' => array(
									'' => '-',
									'1' => 'Grade 1 (0-11)',
									'2' => 'Grade 2 (12-25)',
									'3' => 'Grade 3 (26-44)',
									'4a' => 'Grade 4a (45-60)',
									'4b' => 'Grade 4b (61-80)',
									'5' => 'Grade 5 (81-150)',
									'6' => 'Grade 6 (151-225)',
								),
								'selected' => '',
					),
					'volume_tersertifikasi' => array(
								'column_name' => 'aa.volumeTersertifikasi',
								'type' => 'select',
								'label' => 'Tersertifikasi?',
								'attr' => array(
									'name' => 'volume_tersertifikasi',
									'id' => 'volume_tersertifikasi',
									'class' => 'form-control',
									'value' => '',
								),
								'options' => array(
									'' => '-',
									'YA' => 'YA',
									'TIDAK' => 'TIDAK',
								),
								'selected' => '',
					),
				);
	}

	public function index(){
		$data = null;
		$this->load->view('templates/header', $data);
		$this->load->view('templates/sidebar', $data);
		$this->load->view('templates/topbar', $data);

		$this->load->view('profileKopi/index.php',$data);

		$this->load->view('templates/footer',$data);
	}

	public function searchVolume(){
		$this->blo_search->init($this->search_form,$this->input->post());
		$this->search_form = $this->blo_search->get_current_form();

		if($this->search_form['profil_Kabupaten']['selected']!=''){
			$this->blo_search->update_value($this->search_form,'profil_Kabupaten->options',$this->profileKopi_model->getRegencies($this->search_form['profil_Provinsi']['selected']));
			$this->blo_search->update_value($this->search_form,'profil_Kabupaten->selected',$this->search_form['profil_Kabupaten']['selected']);
		}

		$data['form'] = $this->search_form;
		$data['volume'] = $this->dataVolumeKopi_model->searchVolume($this->search_form);
		$data['title'] = 'Data Volume Biji Kopi yang Dimiliki';

		$this->load->view('templates/header', $data);
		$this->load->view('templates/sidebar', $data);
		$this->load->view('templates/topbar', $data);

		$this->load->view('volumeKopi/data_volume.php',$data);
		$this->load->view('profileKopi/profile_js.php',$data);

		$this->load->view('templates/footer',$data);
	}

	public function searchLogistik(){
		$this->blo_search->init($this->search_form_wilayah,$this->input->post());
		$this->search_form_wilayah = $this->blo_search->get_current_form();

		if($this->search_form_wilayah['profil_Kabupaten']['selected']!=''){
			$this->blo_search->update_value($this->search_form_wilayah,'profil_Kabupaten->options',$this->profileKopi_model->getRegencies($this->search_form_wilayah['profil_Provinsi']['selected']));
			$this->blo_search->update_value($this->search_form_wilayah,'profil_Kabupaten->selected',$this->search_form_wilayah['profil_Kabupaten']['selected']);

			$data['provinsi'] = $this->search_form_wilayah['profil_Provinsi']['selected'];
			$data['kabupaten'] = $this->search_form_wilayah['profil_Kabupaten']['selected'];
		}

		if($this->search_form_wilayah['profil_Kecamatan']['selected']!=''){
			$this->blo_search->update_value($this->search_form_wilayah,'profil_Kecamatan->options',$this->profileKopi_model->getDistricts($this->search_form_wilayah['profil_Provinsi']['selected'],$this->search_form_wilayah['profil_Kabupaten']['selected']));
			$this->blo_search->update_value($this->search_form_wilayah,'profil_Kecamatan->selected',$this->search_form_wilayah['profil_Kecamatan']['selected']);

			$data['kecamatan'] = $this->search_form_wilayah['profil_Kecamatan']['selected'];
		}

		if($this->search_form_wilayah['profil_Kelurahan']['selected']!=''){
			$this->blo_search->update_value($this->search_form_wilayah,'profil_Kelurahan->options',$this->profileKopi_model->getVillages($this->search_form_wilayah['profil_Provinsi']['selected'],$this->search_form_wilayah['profil_Kabupaten']['selected'],$this->search_form_wilayah['profil_Kecamatan']['selected']));
			$this->blo_search->update_value($this->search_form_wilayah,'profil_Kelurahan->selected',$this->search_form_wilayah['profil_Kelurahan']['selected']);

			$data['desa'] = $this->search_form_wilayah['profil_Kelurahan']['selected'];
		}

		if($this->search_form_wilayah['volume_waste']['selected']!='' && strlen($this->search_form_wilayah['volume_waste']['selected'])>0){
			$data['filter']['Waste'] = $this->search_form_wilayah['volume_waste']['selected'];
		}

		if($this->search_form_wilayah['volume_kadarAir']['selected']!='' && strlen($this->search_form_wilayah['volume_kadarAir']['selected'])>0){
			$data['filter']['KadarAir'] = $this->search_form_wilayah['volume_kadarAir']['selected'];
		}

		if($this->search_form_wilayah['volume_beanCount']['selected']!='' && strlen($this->search_form_wilayah['volume_beanCount']['selected'])>0){
			$data['filter']['BeanCount'] = $this->search_form_wilayah['volume_beanCount']['selected'];
		}

		if($this->search_form_wilayah['volume_tersertifikasi']['selected']!='' && strlen($this->search_form_wilayah['volume_tersertifikasi']['selected'])>0){
			$data['filter']['Tersertifikasi'] = $this->search_form_wilayah['volume_tersertifikasi']['selected'];
		}

		$data['form'] = $this->search_form_wilayah;
		$data['volume_area'] = $this->dataVolumeKopi_model->searchVolumeByArea($this->search_form_wilayah,true);
		$data['volume'] = $this->dataVolumeKopi_model->searchVolumeByArea($this->search_form_wilayah);
		$data['table'] = $this->dataVolumeKopi_model->searchUserForVolume($this->search_form_wilayah);
		$data['title'] = 'Data Volume Biji Kopi yang Dimiliki';

		//~ console_html($data['table']);
		$this->load->view('templates/header', $data);
		$this->load->view('templates/sidebar', $data);
		$this->load->view('templates/topbar', $data);

		$this->load->view('volumeKopi/data_logistik.php',$data);
		$this->load->view('profileKopi/profile_js.php',$data);

		$this->load->view('templates/footer',$data);
	}

	public function pembelian(){
		if(empty($_POST)){
			redirect('dataVolumeKopi/searchLogistik');
			return;
		}

		$beli = $this->dataVolumeKopi_model->pembelian($_POST);

		if(!$beli){
			$this->session->set_flashdata('error','Volume Dibeli tidak cocok dengan Volume Tersedia');
		}

		redirect('dataVolumeKopi/searchLogistik');
		return;
	}

	public function getRegencies(){
		if(isset($_POST['province'])){
			echo json_encode($this->profileKopi_model->getRegencies($_POST['province']));
		}
	}
}
?>
