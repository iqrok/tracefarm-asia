<?php defined('BASEPATH') OR exit('No direct script access allowed');

class register extends CI_Controller {
	protected $form;

	public function __construct(){
		parent::__construct();
		//redirect(site_url('Dashboard'));
		$this->load->model('register_model');
		$this->form = array(
					'reg_username' => array(
								'column_name' => 'username',
								'type' => 'input',
								'label' => 'Username',
								'attr' => array(
									'name' => 'reg_username',
									'id' => 'reg_username',
									'class' => 'form-control',
									'placeholder' => 'Username',
									'value' => '',
									'required' => 'required',
								)
					),
					'reg_password' => array(
								'column_name' => 'password',
								'type' => 'password',
								'label' => 'Password',
								'attr' => array(
									'name' => 'reg_password',
									'id' => 'reg_password',
									'class' => 'form-control',
									'placeholder' => 'Password',
									'required' => 'required',
								)
					),
					'reg_confirm_password' => array(
								'column_name' => 'email',
								'type' => 'password',
								'label' => 'Confirm Password',
								'attr' => array(
									'name' => 'reg_confirm_password',
									'id' => 'reg_confirm_password',
									'class' => 'form-control',
									'placeholder' => 'Ketik Ulang Password',
									'required' => 'required',
								)
					),
				);
	}

	public function index(){
		if(!empty($_POST)){
			$data = $_POST;
			$data['reg_password'] = sha1($data['reg_password']);
			$data['reg_confirm_password'] = sha1($data['reg_confirm_password']);

			if(!$this->register_model->check_exist($data['reg_username'])){
				if($data['reg_password'] == $data['reg_confirm_password']){
					$this->register_model->set_form($this->form,$data);
					$this->register_model->register_user($data);
					redirect(site_url('login'));
					return;
				}
				else{
					$data['error'] = "Password tidak sama";
				}
			}
			else
				$data['error'] = "Username sudah digunakan";
		}

		$data['title'] = 'Register';
		$data['button_text'] = 'Daftar';
		$data['form'] = $this->form;

		$this->load->view('login/signup.php',$data);
	}

	public function signup(){
		if(empty($_POST)){
			redirect(site_url('register'));
			return;
		}

		$data = $_POST;
		$data['reg_password'] = sha1($data['reg_password']);
		$this->register_model->set_form($this->form,$data);
		$this->register_model->register_user($data);
	}
}
?>
