<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class login extends CI_Controller {
	protected $redirect_to = 'dashboard';
	protected $redirect_to2 = 'dataVolume/searchLogistik';
	protected $redirect_to_fasilitator = 'sertifikasi';
	
	public function __construct(){
		parent::__construct();		
		$this->load->model('login_model');
		$this->form = array(			
					'reg_username' => array(
								'column_name' => 'username',
								'type' => 'input',
								'label' => 'Username',									
								'attr' => array(
									'name' => 'reg_username',
									'id' => 'reg_username',
									'class' => 'form-control',
									'placeholder' => 'Username',
									'value' => '',
									'required' => 'required',
								)
					),
					'reg_password' => array(
								'column_name' => 'password',
								'type' => 'password',
								'label' => 'Password',									
								'attr' => array(
									'name' => 'reg_password',
									'id' => 'reg_password',
									'class' => 'form-control',
									'placeholder' => 'Password',
									'required' => 'required',
								)
					),
				);
	}
	
	public function index(){		
		if($this->session->userdata('logged_in')){
			if($this->session->userdata('type') == 2)
				redirect(site_url($this->redirect_to));
			else if($this->session->userdata('type') == 5)
				redirect(site_url($this->redirect_to_fasilitator));
			else
				redirect(site_url($this->redirect_to2));
			return;
		}		
		
		if(!empty($_POST)){			
			$username = $_POST['reg_username'];
			$password = $_POST['reg_password'];
			
			$is_valid = $this->login_model->logging_in($username,$password);
			
			if($is_valid == 1){
				if($this->session->userdata('type') == 2)
					redirect(site_url($this->redirect_to));
				else if($this->session->userdata('type') == 5)
					redirect(site_url($this->redirect_to_fasilitator));
				else
					redirect(site_url($this->redirect_to2));
				die();
			}
			
			$data['error'] = "Username / Password Salah";
			$data['form'] = $this->form;
			$this->load->view('login/login',$data);
		}
		else{
			$data['button_text'] = "Login";
			$data['form'] = $this->form;
			$this->load->view('login/login',$data);
		}
	}
	
	public function sign_out(){
		$this->session->unset_userdata('logged_in');
		$this->session->unset_userdata('id');
		$this->session->unset_userdata('username');
		$this->session->unset_userdata('level');
		$this->session->unset_userdata('type');
		$this->session->sess_destroy();
		redirect(site_url('login'));
	}
}