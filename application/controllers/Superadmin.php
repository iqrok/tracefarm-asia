<?php defined('BASEPATH') OR exit('No direct script access allowed');

class superadmin extends CI_Controller {
	protected $form;
	protected $__id;

	public function __construct(){
		parent::__construct();
		$this->__id = isset($_SESSION['id'])?($_SESSION['id']):('');
		$this->load->model('superadmin_model');
		$this->load->model('profile_model');
		$this->load->model('log_model');
		$this->load->model('file_model');
		$this->form = array(
					'profil_jenis' => array(
								'column_name' => 'profilType',
								'type' => 'select',
								'label' => 'Jenis',
								'attr' => array(
									'name' => 'profil_jenis',
									'id' => 'profil_jenis',
									'class' => 'form-control',
									'value' => '',
									'required' => 'required',
								),
								'options' => array(
									'' => '-',
									'1' => 'KOLEKTOR',
									'2' => 'PETANI',
								),
								'selected' => ''
					),
					'profil_nama' => array(
								'column_name' => 'profilNama',
								'type' => 'input',
								'label' => 'Nama Lengkap',
								'attr' => array(
									'name' => 'profil_nama',
									'id' => 'profil_nama',
									'class' => 'form-control',
									'value' => '',
									'required' => 'required',
								)
					),
					'profil_Alamat' => array(
								'column_name' => 'profilAlamat',
								'type' => 'input',
								'label' => 'Alamat',
								'attr' => array(
									'name' => 'profil_Alamat',
									'id' => 'profil_Alamat',
									'class' => 'form-control',
									'value' => '',
									'required' => 'required',
								)
					),
					'profil_Email' => array(
								'column_name' => 'profilEmail',
								'type' => 'input',
								'label' => 'email',
								'attr' => array(
									'name' => 'profil_Email',
									'id' => 'profil_Email',
									'class' => 'form-control',
									'value' => '',
									'required' => 'required',
								)
					),
					'profil_komoditas' => array(
								'column_name' => 'profilKomoditas',
								'type' => 'select',
								'label' => 'Komoditas',
								'attr' => array(
									'name' => 'profil_komoditas',
									'id' => 'profil_komoditas',
									'class' => 'form-control',
									'value' => '',
									'required' => 'required',
								),
								'options' => array(
									'' => '-',
									'kopi' => 'KOPI',
									'rumput_laut' => 'RUMPUT LAUT',
								),
								'selected' => 'kopi'
					),
					'profil_Kelompok' => array(
								'column_name' => 'profilKelompok',
								'type' => 'input',
								'label' => 'Kelompok Tani',
								'attr' => array(
									'name' => 'profil_Kelompok',
									'id' => 'profil_Kelompok',
									'class' => 'form-control ',
									'value' => '',
								)
					),
					'profil_Provinsi' => array(
								'column_name' => 'profilProvinsi',
								'type' => 'input',
								'label' => 'Provinsi',
								'attr' => array(
									'name' => 'profil_Provinsi',
									'id' => 'profil_Provinsi',
									'class' => 'form-control ',
									'value' => '',
								),
					),
					'profil_Kabupaten' => array(
								'column_name' => 'profilKabupaten',
								'type' => 'input',
								'label' => 'Kabupaten',
								'attr' => array(
									'name' => 'profil_Kabupaten',
									'id' => 'profil_Kabupaten',
									'class' => 'form-control ',
									'value' => '',
								),
					),
					'profil_Kecamatan' => array(
								'column_name' => 'profilKecamatan',
								'type' => 'input',
								'label' => 'Kecamatan',
								'attr' => array(
									'name' => 'profil_Kecamatan',
									'id' => 'profil_Kecamatan',
									'class' => 'form-control ',
									'value' => '',
								)
					),
					'profil_Desa' => array(
								'column_name' => 'profilDesa',
								'type' => 'input',
								'label' => 'Desa',
								'attr' => array(
									'name' => 'profil_Desa',
									'id' => 'profil_Desa',
									'class' => 'form-control ',
									'value' => '',
								)
					),
					'profil_JmlKel' => array(
								'column_name' => 'profilJmlAnggotaKeluarga',
								'type' => 'input',
								'label' => 'Jumlah Anggota Keluarga',
								'attr' => array(
									'name' => 'profil_JmlKel',
									'id' => 'profil_JmlKel',
									'class' => 'form-control numOnly',
									'value' => '',
								)
					),
					'profil_sertifikat' => array(
								'column_name' => 'profilHasCert',
								'type' => 'select',
								'label' => 'Bersertifikat',
								'attr' => array(
									'name' => 'profil_sertifikat',
									'id' => 'profil_sertifikat',
									'class' => 'form-control',
									'value' => '',
								),
								'options' => array(
									'' => '-',
									'0' => 'TIDAK',
									'1' => 'IYA',
								),
								'selected' => ''
					),
					'profil_organik' => array(
								'column_name' => 'profilIsOrganic',
								'type' => 'select',
								'label' => 'Organik',
								'attr' => array(
									'name' => 'profil_organik',
									'id' => 'profil_organik',
									'class' => 'form-control',
									'value' => '',
								),
								'options' => array(
									'' => '-',
									'0' => 'TIDAK',
									'1' => 'IYA',
								),
								'selected' => ''
					),
					'profil_thnDepan' => array(
								'column_name' => 'profilPrediksiTahunDepan',
								'type' => 'input',
								'label' => 'Prediksi Tahun Depan (Ton)',
								'attr' => array(
									'name' => 'profil_thnDepan',
									'id' => 'profil_thnDepan',
									'class' => 'form-control numOnly',
									'value' => '',
								)
					),
					'profil_thnlalu' => array(
								'column_name' => 'profilPrediksiTahunLalu',
								'type' => 'input',
								'label' => 'Hasil Setahun Lalu (Ton)',
								'attr' => array(
									'name' => 'profil_thnlalu',
									'id' => 'profil_thnlalu',
									'class' => 'form-control numOnly',
									'value' => '',
								)
					),
					'user_id' => array(
								'column_name' => 'profilUserId',
								'type' => 'data',
								'attr' => array(
									'name' => 'user_id',
									'value' => (isset($_SESSION['editUserId'])?($_SESSION['editUserId']):('')),
									)
					),
				);
		
		$this->form_kebun = array(
					'user_id' => array(
								'column_name' => 'kebunUserId',
								'type' => 'data',
								'attr' => array(
									'name' => 'user_id',
									'value' => (isset($_SESSION['editUserId'])?($_SESSION['editUserId']):('')),
									)
					),
					'kebun_ukuran' => array(
								'column_name' => 'kebunUkuran',
								'type' => 'input',
								'label' => 'Ukuran Kebun (Ha)',
								'attr' => array(
									'name' => 'kebun_ukuran',
									'id' => 'kebun_ukuran',
									'class' => 'form-control',
									'value' => '',
									'required' => 'required',
								),
					),
					'kebun_koordinat' => array(
								'column_name' => 'kebunKoordinat',
								'type' => 'input',
								'label' => 'Koordinat GPS Kebun',
								'attr' => array(
									'name' => 'kebun_koordinat',
									'id' => 'kebun_koordinat',
									'class' => 'form-control',
									'value' => '',
								),
					),
					'kebun_sertifikasi' => array(
								'column_name' => 'kebunSertifikasi',
								'type' => 'select',
								'label' => 'Tersertifikasi',
								'attr' => array(
									'name' => 'kebun_sertifikasi',
									'id' => 'kebun_sertifikasi',
									'class' => 'form-control',
									'value' => '',
								),
								'options' => array(
									'' => '-',
									'0' => 'TIDAK',
									'1' => 'IYA',
								),
								'selected' => ''
					),
				);
				
		$this->form_volume = array(
					'user_id' => array(
								'column_name' => 'volumeUserId',
								'type' => 'data',
								'attr' => array(
									'name' => 'user_id',
									'value' => (isset($_SESSION['editUserId'])?($_SESSION['editUserId']):('')),
									)
					),
					'volume_timestamp' => array(
								'column_name' => 'volumeTimestamp',
								'type' => 'data',
								'attr' => array(
									'name' => 'volume_timestamp',
									'value' => time(),
									)
					),
					'volume_waste' => array(
								'column_name' => 'volumeWaste',
								'type' => 'select',
								'label' => 'Waste (%)',
								'attr' => array(
									'name' => 'volume_waste',
									'id' => 'volume_waste',
									'class' => 'form-control',
									'required' => 'required',
									'value' => '',
								),
								'options' => array(
									'' => '-',
									'2.5' => '2,5%',
									'3' => '3%',
									'4' => '4%',
									'5' => '5%',
									'6' => '6%',
									'7' => '7%',
									'8' => '8%',
									'9' => '9%',
									'10' => '10%',
								),
								'selected' => '',
					),
					'volume_kadarAir' => array(
								'column_name' => 'volumeKadarAir',
								'type' => 'select',
								'label' => 'Kadar Air (%)',
								'attr' => array(
									'name' => 'volume_kadarAir',
									'id' => 'volume_kadarAir',
									'class' => 'form-control',
									'required' => 'required',
									'value' => '',
								),
								'options' => array(
									'' => '-',
									'7' => '7%',
									'8' => '8%',
									'9' => '9%',
									'10' => '10%',
								),
								'selected' => '',
					),
					'volume_beanCount' => array(
								'column_name' => 'volumeBeanCount',
								'type' => 'select',
								'label' => 'Bean Count',
								'attr' => array(
									'name' => 'volume_beanCount',
									'id' => 'volume_beanCount',
									'class' => 'form-control',
									'required' => 'required',
									'value' => '',
								),
								'options' => array(
									'' => '-',
									'AA' => 'AA (<85 biji)',
									'A' => 'A (85-100 biji)',
									'B' => 'B (101-110 biji)',
									'C' => 'C (111-120 biji)',
									'S' => 'D (>120 biji)',
								),
								'selected' => '',
					),
					'volume_weight' => array(
								'column_name' => 'volumeWeight',
								'type' => 'input',
								'label' => 'Volume (ton)',
								'attr' => array(
									'name' => 'volume_weight',
									'id' => 'volume_weight',
									'class' => 'form-control',
									'required' => 'required',
									'value' => '',
								),
					),
				);
				
		$this->form_file = array(
					'file_id' => array(
								'column_name' => 'fileId',
								'type' => 'data',
								'attr' => array(
									'name' => 'file_id',
									'value' => '',
									)
					),
					'file_type' => array(
								'column_name' => 'fileType',
								'type' => 'select',
								'label' => 'Tipe File',
								'attr' => array(
									'name' => 'file_type',
									'id' => 'file_type',
									'class' => 'form-control',
									'value' => '',
									'required' => 'required',
								),
								'options' => array(
									'' => '-',
									'video' => 'video',
									'doc' => 'document',
								),
								'selected' => ''
					),
					'file_title' => array(
								'column_name' => 'fileTitle',
								'type' => 'input',
								'label' => 'Judul File',
								'attr' => array(
									'name' => 'file_title',
									'id' => 'file_title',
									'class' => 'form-control',
									'value' => '',
									'required' => 'required',
								),
					),
					'file_url' => array(
								'column_name' => 'fileURL',
								'type' => 'input',
								'label' => 'URL Google Drive File',
								'attr' => array(
									'name' => 'file_url',
									'id' => 'file_url',
									'class' => 'form-control',
									'value' => '',
									'required' => 'required',
								),
					),
				);
	}

	public function index(){
		if(!isset($_SESSION['alert'])){
			update_value_array($this->form,'profil_jenis->attr->disabled','disabled');
		}

		if(isset($_SESSION['type']) && $_SESSION['type']==3){
			update_value_array($this->form,'profil_jenis->options',array('3'=>'PEMBORONG'));
			update_value_array($this->form,'profil_jenis->selected','3');
		}
		
		$this->profile_model->set_form($this->form);
		$data = $this->profile_model->get_profile($this->__id);

		$data['title'] = 'Telusur';
		$data['form'] = $this->profile_model->get_form();
		
		$this->profile_model->set_form($this->form_kebun);
		$data['form_kebun'] = $this->profile_model->get_form();
		$data['list_kebun'] = $this->profile_model->listKebun($this->__id);
		
		$this->load->view('templates/header', $data);
		$this->load->view('templates/sidebar', $data);
		$this->load->view('templates/topbar', $data);

		$this->load->view('profile/view_profile.php',$data);
		$this->load->view('profile/profile_js.php',$data);

		$this->load->view('templates/footer',$data);
	}

	public function editUser($userId,$type){
		if(!isset($_SESSION['alert'])){
			update_value_array($this->form,'profil_jenis->attr->disabled','disabled');
		}

		//~ if(isset($type) && $type==3){
			//~ update_value_array($this->form,'profil_jenis->options',array('3'=>'PEMBORONG'));
			//~ update_value_array($this->form,'profil_jenis->selected','3');
		//~ }
		
		$this->profile_model->set_form($this->form);
		$data = $this->profile_model->get_profile($userId);

		$data['title'] = 'Telusur';
		$data['form'] = $this->profile_model->get_form();
		
		$_SESSION['editUserId'] = $userId;
		
		$this->profile_model->set_form($this->form_kebun);
		$data['form_kebun'] = $this->profile_model->get_form();
		$data['list_kebun'] = $this->profile_model->listKebun($userId);
				
		$this->load->view('templates/header', $data);
		$this->load->view('templates/sidebar', $data);
		$this->load->view('templates/topbar', $data);

		$this->load->view('profile/view_profile.php',$data);
		$this->load->view('profile/profile_js.php',$data);

		$this->load->view('templates/footer',$data);
	}

	public function editKebun($userId){
		$_SESSION['editUserId'] = $userId;
		$this->superadmin_model->set_form($this->form_kebun);

		$userProfile = $this->superadmin_model->get_profile_raw($userId);
		
		$data['form_kebun'] = $this->superadmin_model->get_form();
		$data['list_kebun'] = $this->superadmin_model->listKebun($userId);
		$data['title'] = 'Data Kebun - ['.$userId.'] '.$userProfile['profilNama'] ;

		$this->load->view('templates/header', $data);
		$this->load->view('templates/sidebar', $data);
		$this->load->view('templates/topbar', $data);

		$this->load->view('profile/list_kebun.php',$data);
		$this->load->view('profile/profile_js.php',$data);

		$this->load->view('templates/footer',$data);
	}

	public function dataVolumeKakao(){
		$data['table'] = $this->superadmin_model->listVolume();
		$data['title'] = 'Rekam Volume Biji Kakao yang Tersimpan';

		//~ if($_SESSION['type'] == 4)
			//~ $data['title'] = 'Data Volume Biji Kakao yang Tersimpan';

		$this->load->view('templates/header', $data);
		$this->load->view('templates/sidebar', $data);
		$this->load->view('templates/topbar', $data);

		$this->load->view('superadmin/data_volume_all.php',$data);

		$this->load->view('templates/footer',$data);
	}
	
	public function list_all_users($isPetani=null){
		$data['list'] = $this->superadmin_model->listAllUsers($isPetani);
		$data['title'] = ($isPetani==null)?("Daftar Semua Mitra"):("Daftar Semua Petani");
		//~ console_html($data);
		
		$this->load->view('templates/header', $data);
		$this->load->view('templates/sidebar', $data);
		$this->load->view('templates/topbar', $data);

		$this->load->view('superadmin/list_all_users.php',$data);

		$this->load->view('templates/footer',$data);
	}

	public function save_profile(){
		$userId = $_SESSION['editUserId'];
		//~ die($userId);
		if(empty($_POST) || !isset($_SESSION['editUserId'])){
			redirect(site_url('superadmin/list_all_users'));
			unset($_SESSION['editUserId']);
			return;
		}

		$this->superadmin_model->save_profile($userId,$this->form,$_POST);
		redirect(site_url('superadmin/list_all_users'));
	}

	public function save_kebun(){
		$userId = $_SESSION['editUserId'];		
		update_value_array($this->form_kebun,'user_id->value',$userId);
		if(empty($_POST)){
			redirect(site_url('profile'));
			return;
		}

		$this->superadmin_model->save_kebun($userId,$this->form_kebun,$_POST);
		unset($_SESSION['editUserId']);
		redirect(site_url('superadmin/list_all_users'));
	}

	public function save_volume(){
		$userId = $_SESSION['editUserId'];
		update_value_array($this->form_volume,'user_id->value',$userId);
		if(empty($_POST)){
			redirect(site_url('databaseKebun/dataVolumeKakao'));
			return;
		}

		$this->superadmin_model->save_volume($userId,$this->form_volume,$_POST);
		unset($_SESSION['editUserId']);
		redirect(site_url('databaseKebun/dataVolumeKakao'));
	}

	public function list_files(){
		$data['list'] = $this->file_model->list_files();
		
		$this->load->view('templates/header', $data);
		$this->load->view('templates/sidebar', $data);
		$this->load->view('templates/topbar', $data);

		$this->load->view('superadmin/list_files.php',$data);

		$this->load->view('templates/footer',$data);
	}

	public function save_file(){
		if(empty($_POST)){
			redirect(site_url('superadmin/list_files'));
			return;
		}

		$this->file_model->save_file($this->form_file,$_POST);
		redirect(site_url('superadmin/list_files'));
	}

	public function delete_file($fileId){
		if($_SESSION['type']!=7){
			redirect(site_url('superadmin/list_files'));
			return;
		}

		$this->file_model->delete_file($fileId);
		redirect(site_url('superadmin/list_files'));
	}


	public function getRegencies(){
		if(isset($_POST['province'])){
			echo json_encode($this->profile_model->getRegencies($_POST['province']));
		}
	}

	public function getDistricts(){
		if(isset($_POST['province']) && isset($_POST['kabupaten'])){
			echo json_encode($this->profile_model->getDistricts($_POST['province'],$_POST['kabupaten']));
			//~ echo $this->profile_model->getDistricts($_POST['province'],$_POST['kabupaten']);
			//~ echo "wut";
		}
		else{
			//~ echo ":asasasa";
			echo json_encode($this->profile_model->getDistricts("SULAWESI SELATAN","PINRANG"));
		}
	}

	public function getVillages(){
		if(isset($_POST['province']) && isset($_POST['kabupaten']) && isset($_POST['kecamatan'])){
			echo json_encode($this->profile_model->getVillages($_POST['province'],$_POST['kabupaten'],$_POST['kecamatan']));
			//~ echo $this->profile_model->getDistricts($_POST['province'],$_POST['kabupaten']);
			//~ echo "wut";
		}
		else{
			//~ echo ":asasasa";
			echo json_encode($this->profile_model->getVillages("SULAWESI SELATAN","PINRANG","LEMBANG"));
		}
	}

	public function insertUpdateNewFile($fileId=null){		
		$this->superadmin_model->set_form($this->form_file);
		//~ $data = $this->superadmin_model->get_file($fileId);

		$data['title'] = 'Insert New File';
		$data['form'] = $this->superadmin_model->get_form();		
		
		$this->load->view('templates/header', $data);
		$this->load->view('templates/sidebar', $data);
		$this->load->view('templates/topbar', $data);

		$this->load->view('superadmin/view_file.php',$data);
		
		$this->load->view('templates/footer',$data);
	}
	
	public function delete_kebun($kebunId){
		$this->profile_model->delete_kebun($kebunId);
		redirect(site_url('superadmin/list_all_users'));
	}

	public function delete_log($logId){
		$this->log_model->delete_log($logId);
		redirect(site_url('log'));
	}
}
?>
