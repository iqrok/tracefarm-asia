<?php defined('BASEPATH') OR exit('No direct script access allowed');

class blo_form {
	protected $CI;
	public $form;

	public function __construct()
	{
		$this->CI =& get_instance();
	}
	
	public function init($form){
		$this->set_form($form);
	}

	// these methods should be in a different & global class -------------------------
	public function set_form($form){
		$this->form = $form;
	}
	
	public function get_form(){
		return $this->form;
	}
	
	public function get_form_value($index){
		switch($this->form[$index]['type']){
			case 'data':
			case 'input':
			case 'password':
				return get_value_array($this->form,$index.'->attr->value');
				break;
			case 'select':
				return get_value_array($this->form,$index.'->selected');
				break;
			case 'label':
				return get_value_array($this->form,$index.'->text');
				break;
			default: 
				break;
		}
		
		return false;
	}
	
	protected function get_index_from_column_name($index){
		foreach($this->form as $key=>$val){
			if(isset($val['column_name']) && $val['column_name'] == $index)
				return $key;
		}
		
		return false;
	}
		
	public function update_form_value($index,$value){
		switch($this->form[$index]['type']){
			case 'data':
			case 'input':
			case 'password':
				update_value_array($this->form,$index.'->attr->value',$value);
				break;
			case 'select':
				update_value_array($this->form,$index.'->selected',$value);
				break;
			case 'label':
				update_value_array($this->form,$index.'->text',$value);
				break;
			default: break;
		}
	}
	
	public function update_form($data,$fromDB=false){
		foreach($data as $key=>$value){
			if($fromDB)
				$this->update_form_value($this->get_index_from_column_name($key),$value);
			else
				$this->update_form_value($key,$value);
		}
	}
		
	public function set_form_value_from_db($form_items){
		
		foreach($form_items as $key=>$val){
			$index_name = $this->get_index_from_column_name($key);
			
			if($index_name !== FALSE){
				//~ echo $key." => ".$index_name." = ".$val."<br>";
				$this->update_form_value($index_name,$val);
			}
		}
	}
		
	public function set_select_options($index,$options){
		if($this->form[$index]['type'] != 'select')
			return false;
		
		update_value_array($this->form,$index.'->options',$options);
		return true;		
	}
	
	public function get_grouped_values($table,$col_name,$col_value=NULL,$slug=NULL,$isEmptyNeeded=true){
		/*
		 * this function is used to get the options for html select input
		 * */
		if($col_value == NULL)
			$col_value = $col_name;
			
		$this->db->select($col_value.','.$col_name);
		$this->db->from($table);
		
		//slug should be in assoc. array => array(haystack_col_name,needle)
		if($slug != NULL)
			$this->db->where($slug[0].'=',$slug[1]);
			
		$this->db->group_by($col_name);
		
		$tmp = $this->db->get()->result_array();
		$result = [];
		$empty_row_exist = 0;
		foreach($tmp as $val){
			$result[$val[$col_value]] = ($val[$col_name]=='')?("[KOSONG]"):($val[$col_name]);
			
			if($val[$col_name]=='')
				$empty_row_exist = 1;
		}
		
		if($empty_row_exist || $isEmptyNeeded)
			$result = array('' => "[KOSONG]") + $result;
			
		return $result;
	}
	//-----------------------------------------------------------------------------------------
}
