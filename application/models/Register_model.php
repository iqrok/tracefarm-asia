<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class register_model extends CI_Model {
	protected $form;
	protected $username;
	protected $password;
	protected $email;
	
	public function __construct(){
		$this->load->database();
		$this->load->library('blo_form');
	}

	public function set_form($form,$data=null){
		$this->blo_form->init($form);

		if($data!=null)
			$this->blo_form->update_form($data);

		$this->form = $this->blo_form->get_form();
		return;
	}

	public function get_form(){
		return $this->form;
	}

	public function check_exist($username){
		$this->db->select('username');
		$this->db->from('tb_user');
		$this->db->where('username = ',$username);
		$query = $this->db->get();
		return ($query->num_rows()>0)?(true):(false);
	}
	
	public function register_user($data){
		$this->insert_update_db("tb_user",$data,"insert");
	}
	
	public function insert_update_db($table,$data,$action){
		$action = strtolower($action);
						
		foreach($this->form as $key=>$val){
			if(isset($data[$key])){
				$this->db->set($val['column_name'],$data[$key]);
			}
		}
		
		switch($action){
			case 'insert':			
				$this->db->insert($table);
				break;
				
			case 'update':
				$this->db->where('username',$data['username']);
				$this->db->update($table);
				break;
		}	
	}
}
