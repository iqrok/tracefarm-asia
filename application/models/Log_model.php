<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class log_model extends CI_Model {
	protected $form;

	public function __construct(){
		$this->load->database();
		$this->load->library('blo_form');
	}

	public function get_receiver_list($userType){
		if($userType==1)
			$receiverType = 2;
		else
			$receiverType = 1;

		$this->db->select('
							profilUserId AS RECEIVER_ID,
							profilNama AS NAME
						  ');
		$this->db->from('tb_user_profile');
		$this->db->where('profilType = ',$receiverType);
		$data['receiverList'] = $this->db->get()->result_array();

		$this->db->select('profTypeName AS TYPE');
		$this->db->from('tb_profil_type');
		$this->db->where('profTypeId = ',$receiverType);
		$data['receiverType'] = $this->db->get()->result_array()[0]['TYPE'];
		
		return $data;
	}

	public function save_form_log($form,$data){
		$this->set_form($form,$data);
		
		$this->insert_update_db("tb_log",$data,'insert');
		return;
	}
	
	public function set_form($form,$data=null,$fromDB=false){
		$this->blo_form->init($form);
		
		if($data!=null)
			$this->blo_form->update_form($data,$fromDB);

		$this->form = $this->blo_form->get_form();
		return;
	}

	public function get_form(){
		return $this->form;
	}
	
	public function insert_update_db($table,$data,$action){
		$action = strtolower($action);
						
		foreach($this->form as $key=>$val){
			if(isset($data[$key])){
				if(strpos($val['attr']['class'],'datepicker') !== false)
					$data[$key] = strtotime($data[$key]);

				$this->db->set($val['column_name'],$data[$key]);
			}
			else if($val['type']=='data'){				
				$this->db->set($val['column_name'],$val['attr']['value']);
			}
		}
		
		switch($action){
			case 'insert':			
				$this->db->insert($table);
				break;
				
			case 'update':
				$this->db->where('profilUserId',$_SESSION['id']);
				$this->db->update($table);
				break;
		}	
	}

	public function get_log($userId,$month=null,$year=null,$groupBy=null){
	    $this->db->select('bb.profTypeId,bb.profTypeName');
	    $this->db->from('tb_user_profile AS aa');
	    $this->db->join('tb_profil_type AS bb','aa.profilType=bb.profTypeId');
	    $this->db->where('aa.profilUserId =',$userId); 
	    $userType = $this->db->get()->result_array()[0];
	    
	    if(strtoupper($userType['profTypeName']) == 'KOLEKTOR')
	        return $this->get_log_reversed($userId,$month,$year,$groupBy);
	    
		$this->db->select('
							aa.logId,
							aa.logReceiverId,
							bb.profilNama,
							cc.profTypeName,
							aa.logTimestamp,
							FROM_UNIXTIME(aa.logTimestamp,"%M-%Y") AS MONTHYEAR,
							SUM(aa.logVolume) AS logVolume');
		$this->db->from('tb_log AS aa');
		$this->db->join('tb_user_profile AS bb','aa.logReceiverId = bb.profilUserId');
		$this->db->join('tb_profil_type AS cc','cc.profTypeId = bb.profilType');
		$this->db->where('aa.logUserId = ',$userId);

		if($year!==false && $month!==false){
			if($year == null){
				$year = date('Y');
			}

			if($month == null){
				$month = date('m');
			}

			$firstDateOfMonth = $year.'-'.$month.'-'.'01 00:00:00';
			$lastDateOfMonth = $year.'-'.$month.'-'.date('t',strtotime($firstDateOfMonth)).' 23:59:59';
			$this->db->where('aa.logTimestamp >= ',strtotime($firstDateOfMonth));
			$this->db->where('aa.logTimestamp <= ',strtotime($lastDateOfMonth));
		}
		
		switch($groupBy){
			case 1:
				$this->db->group_by('bb.profilNama');
			break;

			case 2:
				$this->db->group_by('FROM_UNIXTIME(aa.logTimestamp,"%M")');
				$this->db->group_by('FROM_UNIXTIME(aa.logTimestamp,"%Y")');
				$this->db->order_by('aa.logTimestamp');
			break;
			
			case 3;
				$this->db->group_by('aa.logTimestamp');
			break;
			
			default:
			break;
		}
		
		
		return $this->db->get()->result_array();
	}

	public function get_log_reversed($userId,$month=null,$year=null,$groupBy=null){
		$this->db->select('
							aa.logId,
							aa.logUserId,
							bb.profilNama,
							cc.profTypeName,
							aa.logTimestamp,
							FROM_UNIXTIME(aa.logTimestamp,"%M-%Y") AS MONTHYEAR,
							SUM(aa.logVolume) AS logVolume');
		$this->db->from('tb_log AS aa');
		$this->db->join('tb_user_profile AS bb','aa.logUserId = bb.profilUserId');
		$this->db->join('tb_profil_type AS cc','cc.profTypeId = bb.profilType');
		$this->db->where('aa.logReceiverId = ',$userId);
		
		if($year!==false && $month!==false){
			if($year == null){
				$year = date('Y');
			}

			if($month == null){
				$month = date('m');
			}

			$firstDateOfMonth = $year.'-'.$month.'-'.'01 00:00:00';
			$lastDateOfMonth = $year.'-'.$month.'-'.date('t',strtotime($firstDateOfMonth)).' 23:59:59';
			$this->db->where('aa.logTimestamp >= ',strtotime($firstDateOfMonth));
			$this->db->where('aa.logTimestamp <= ',strtotime($lastDateOfMonth));
		}
		
		switch($groupBy){
			case 1:
				$this->db->group_by('bb.profilNama');
			break;

			case 2:
				$this->db->group_by('FROM_UNIXTIME(aa.logTimestamp,"%M")');
				$this->db->group_by('FROM_UNIXTIME(aa.logTimestamp,"%Y")');
				$this->db->order_by('aa.logTimestamp');
			break;
			
			case 3;
				$this->db->group_by('aa.logTimestamp');
			break;
			
			default:
			break;
		}
		
		return $this->db->get()->result_array();
	}

	public function delete_log($logId){
		$this->db->where('logId', $logId);
		$this->db->delete('tb_log');

		return;
	}
}
