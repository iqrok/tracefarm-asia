<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class file_model extends CI_Model {
	public function __construct(){
		$this->load->database();
		$this->load->library('blo_form');
	}

	protected function get_file_id($url){
		$needle = 'open?id=';
		$index = strpos($url,$needle);

		if($index>0)
			return substr($url,($index+strlen($needle)));
		else{
			$needle = "https://drive.google.com/file/d/";
			$index = strpos($url,$needle);
			$substr_index = $index+strlen($needle);
			$index_slash = strpos($url,"/",$substr_index);
			$id = substr($url,$substr_index,($index_slash-$substr_index));

			return $id;
		}
	}

	public function get_files_from_db(){
		$this->db->select('*');
		$this->db->from('tb_file');
		$this->db->order_by('fileTitle ASC');
		$data = $this->db->get()->result_array();

		$files = array();
		$idx = array();
		foreach($data as $key=>$val){
			if(!isset($idx[$val['fileType']]))
				$idx[$val['fileType']]=0;

			$val['id'] = $this->get_file_id($val['fileURL']);
			$val['iframe'] = ($val['fileType']=='video')?('<iframe src="https://drive.google.com/file/d/'.$val['id'].'/preview" width="320" height="240"></iframe>'):(null);
			$files[$val['fileType']][$idx[$val['fileType']]++]=$val;
		}
		return $files;
	}

	public function set_form($form,$data=null,$fromDB=false){
		$this->blo_form->init($form);

		if($data!=null)
			$this->blo_form->update_form($data,$fromDB);

		$this->form = $this->blo_form->get_form();
		return;
	}

	public function get_form(){
		return $this->form;
	}

	public function insert_gdrive_url($data,$action,$where='fileId',$table='tb_file'){
		$action = strtolower($action);

		foreach($this->form as $key=>$val){
			if(isset($data[$key])){
				$this->db->set($val['column_name'],$data[$key]);
			}
			else if($val['type']=='data'){
				if($val['attr']['value'] != '')
					$this->db->set($val['column_name'],$val['attr']['value']);
			}
		}

		switch($action){
			case 'insert':
				$this->db->insert($table);
				break;

			case 'update':
				$this->db->where($where,$data['file_id']['attr']['value']);
				$this->db->update($table);
				break;
		}
	}

	public function save_file($form,$data){
		$this->set_form($form,$data);

		$action = 'insert';

		$this->insert_gdrive_url($data,$action);

		return;
	}

	public function list_files(){
		$this->db->select('*');
		$this->db->from('tb_file');
		$this->db->order_by('fileType,fileTitle');

		return $this->db->get()->result_array();
	}

	public function delete_file($fileId){
		$this->db->where('fileId', $fileId);
		$this->db->delete('tb_file');

		return;
	}
}
