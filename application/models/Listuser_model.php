<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class listuser_model extends CI_Model {	
	public function __construct(){
		$this->load->database();
	}

	public function list_user($type){
		$this->db->select('*');
		$this->db->from('tb_user_profile');
		$this->db->where('profilType =',$type);
		$this->db->order_by('profilNama ASC');
		return $this->db->get()->result_array();
	}
}
