<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class sertifikasi_model extends CI_Model {	
	public function __construct(){
		$this->load->database();
	}

	public function checkSertifikasiExist($userId){
		$this->db->select('usrsertSertId ');
		$this->db->from('tb_user_sertifikasi');
		$this->db->where('usrsertUserId = ',$userId);
		
		$result = $this->db->count_all_results();
		return $result;
	}

	public function getAllPetani(){
		$this->db->select('bb.username,profilUserId,profilNama');
		$this->db->from('tb_user_profile');
		$this->db->join('tb_user bb','bb.id = profilUserId');
		$this->db->where('profilType=2');
		$this->db->order_by('bb.username ASC,profilNama ASC');		
		$listUser = $this->db->get()->result_array();
		
		$retVal = array();
		$i=0;
		foreach($listUser as $key=>$val){
			$retVal[$i++] = array(
									'key' => $val['profilUserId'],
									'val' => "[ ".$val['username']." ] ".$val['profilNama'],
								);
		}
		return $retVal;
	}

	public function initSertifikasi($userId){
		$this->db->select('sertId');
		$this->db->from('tb_form_sertifikasi');
		$this->db->order_by('sertId ASC');		
		$listSertId = $this->db->get()->result_array();
		
		$batchInsert = array();
		$i=0;
		foreach($listSertId as $key=>$val){
			$batchInsert[$i++] = array(
									'usrsertUserId' => $userId,
									'usrsertSertId' => $val['sertId'],
								);
		}
		//~ var_dump($batchInsert);
		$this->db->insert_batch('tb_user_sertifikasi', $batchInsert);
		return;
	}

	public function getFormSertifikasi($userId){
		$sertClass = array(
				'CR' => 'Critical',
				'A' => 'A',
				'B' => 'B',
				'C' => 'C',
			);
		$this->db->select('
							bb.sertId AS SERT_ID,
							bb.sertClass AS SERT_CLASS,
							bb.sertQuestion AS QUESTION,
							cc.tysertName AS TYPE,
							cc.tysertId AS TYPE_ID,
							dd.prinName AS PRINCIPLE,
							dd.prinId AS PRINCIPLE_ID,
						 ');
		//~ $this->db->from('tb_user_sertifikasi AS aa');
		//~ $this->db->join('tb_form_sertifikasi AS bb','aa.usrsertSertId = bb.sertId');
		$this->db->from('tb_form_sertifikasi AS bb');
		$this->db->join('tb_type_sertifikasi AS cc','bb.sertType = cc.tysertId');
		$this->db->join('tb_principle_sertifikasi AS dd','cc.tysertPrinId = dd.prinId');
		//~ $this->db->where('aa.usrsertUserId = ',$userId);
		//~ $this->db->order_by('aa.usrsertSertId ASC');		
		$list = $this->db->get()->result_array();

		$formSertifikasi = array();

		foreach($list as $key=>$val){
			$formSertifikasi[$val['PRINCIPLE_ID']]['NAME'] = $val['PRINCIPLE'];
			$formSertifikasi[$val['PRINCIPLE_ID']]['TYPE'][$val['TYPE_ID']]['NAME'] = $val['TYPE'];
			$formSertifikasi[$val['PRINCIPLE_ID']]['TYPE'][$val['TYPE_ID']]['QUESTION'][$val['SERT_ID']] = array(
																									'TEXT' => $val['QUESTION'],
																									'CLASS' => $sertClass[$val['SERT_CLASS']],
																									);
		}	
		
		//~ var_dump($formSertifikasi);
		return $formSertifikasi;
	}

	public function listUserTersetifikasi(){
		$query = $this->db->query('SELECT aa.*,cc.*
			FROM tb_user_profile aa
			JOIN (SELECT usrsertUserId FROM tb_user_sertifikasi GROUP BY usrsertUserId) bb ON aa.profilUserId=bb.usrsertUserId
			JOIN tb_profil_type cc ON aa.profilType = cc.profTypeId
		');
		$retval = $query->result_array();
		for($i=0; $i<sizeof($retval); $i++){
		    $retval[$i]['hasil'] = $this->parseHasil( $this->penilaian_sertifikasi($retval[$i]['profilUserId']) );
		}
		// var_dump($retval);
		return $retval;
	}

	public function getHasilSertifikasi($userId){
		$sertClass = array(
				'CR' => 'Critical',
				'A' => 'A',
				'B' => 'B',
				'C' => 'C',
			);
		$this->db->select('
							bb.sertId AS SERT_ID,
							bb.sertClass AS SERT_CLASS,
							bb.sertQuestion AS QUESTION,
							cc.tysertName AS TYPE,
							cc.tysertId AS TYPE_ID,
							dd.prinName AS PRINCIPLE,
							dd.prinId AS PRINCIPLE_ID,
							aa.usrsertValue AS VALUE
						 ');
		$this->db->from('tb_user_sertifikasi AS aa');
		$this->db->join('tb_form_sertifikasi AS bb','aa.usrsertSertId = bb.sertId');
		//~ $this->db->from('tb_form_sertifikasi AS bb');
		$this->db->join('tb_type_sertifikasi AS cc','bb.sertType = cc.tysertId');
		$this->db->join('tb_principle_sertifikasi AS dd','cc.tysertPrinId = dd.prinId');
		$this->db->where('aa.usrsertUserId = ',$userId);
		//~ $this->db->order_by('aa.usrsertSertId ASC');
		//~ echo $this->db->get_compiled_select('',false);		
		$list = $this->db->get()->result_array();

		$formSertifikasi = array();

		foreach($list as $key=>$val){
			$formSertifikasi[$val['PRINCIPLE_ID']]['NAME'] = $val['PRINCIPLE'];
			$formSertifikasi[$val['PRINCIPLE_ID']]['TYPE'][$val['TYPE_ID']]['NAME'] = $val['TYPE'];
			$formSertifikasi[$val['PRINCIPLE_ID']]['TYPE'][$val['TYPE_ID']]['QUESTION'][$val['SERT_ID']] = array(
																									'TEXT' => $val['QUESTION'],
																									'CLASS' => $sertClass[$val['SERT_CLASS']],
																									'VALUE' => $val['VALUE'],
																									);
		}	
		
		//~ var_dump($formSertifikasi);
		return $formSertifikasi;
	}

	public function penilaian_sertifikasi($userId){
		$this->db->select('bb.sertClass AS CLASS, COUNT(*) AS total');
		$this->db->from('tb_user_sertifikasi AS aa');
		$this->db->join('tb_form_sertifikasi AS bb','aa.usrsertSertId = bb.sertId');
		$this->db->where('aa.usrsertValue = "C"');
		$this->db->where('aa.usrsertUserId = ',$userId);
		$this->db->group_by('bb.sertClass');		
		$listPenilaian = $this->db->get()->result_array();
		
		$this->db->select('bb.sertClass AS CLASS, COUNT(*) AS total');
		$this->db->from('tb_form_sertifikasi AS bb');
		$this->db->group_by('bb.sertClass');	
		$listAcuan = $this->db->get()->result_array();	

		$listHasil = array();
		foreach($listPenilaian AS $key=>$val){
			$listHasil[$val['CLASS']]['nilai'] = $val['total'];
		}
		
		foreach($listAcuan AS $key=>$val){
			$listHasil[$val['CLASS']]['acuan'] = $val['total'];
			$listHasil[$val['CLASS']]['hasil'] = $listHasil[$val['CLASS']]['nilai'] / $listHasil[$val['CLASS']]['acuan'] *100;
		}
		
		return $listHasil;
	}

	public function parseHasil($penilaian_sertifikasi){
		$minNilai = array(
			0 => array(
				'CR' => 100,
				'C' => 50,
				'B' => 0,
				'A' => 0,
			),
			1 => array(
				'CR' => 100,
				'C' => 65,
				'B' => 0,
				'A' => 0,
			),
			2 => array(
				'CR' => 100,
				'C' => 80,
				'B' => 0,
				'A' => 0,
			),
			3 => array(
				'CR' => 100,
				'C' => 100,
				'B' => 50,
				'A' => 0,
			),
			4 => array(
				'CR' => 100,
				'C' => 100,
				'B' => 65,
				'A' => 0,
			),
			5 => array(
				'CR' => 100,
				'C' => 100,
				'B' => 80,
				'A' => 0,
			),
			6 => array(
				'CR' => 100,
				'C' => 100,
				'B' => 100,
				'A' => 50,
			),
		);

		$isLulus = true;
		foreach($minNilai[0] as $key => $val){
			if($penilaian_sertifikasi[$key]['hasil'] < $val && $isLulus){
				$isLulus = false;
			}
		}

		return $isLulus;
	}
		
	public function save_form_sertifikasi($userId,$form){
		$sertifikasi_values = array();
		$idx = 0;			
		foreach($form as $key=>$val){
			$sertifikasi_values[$idx++] = $val;
			$this->db->set('usrsertValue',$val);
			$this->db->where('usrsertSertId',$key);
			$this->db->where('usrsertUserId',$userId);
			$result= $this->db->update('tb_user_sertifikasi');
		}
		//~ sort($sertifikasi_values);
		//~ $_SESSION['sertifikasi-report']['userId'] = $this->parseHasil($this->penilaian_sertifikasi($userId));
		//~ $_SESSION['sertifikasi-report']['hasil'] = $this->parseHasil($this->penilaian_sertifikasi($userId));
		//~ console_html($this->parseHasil($this->penilaian_sertifikasi($userId)));
		
		return;
	}

	public function insert_update_db($table,$data,$action){
		$action = strtolower($action);
						
		foreach($this->form as $key=>$val){
			if(isset($data[$key])){
				$this->db->set($val['column_name'],$data[$key]);
			}
		}
		
		switch($action){
			case 'insert':			
				$this->db->insert($table);
				break;
				
			case 'update':
				$this->db->where('karNoPegawai',$data['noPegawai']);
				$this->db->update($table);
				break;
		}	
	}
}
