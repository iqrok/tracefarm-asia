<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class dataVolumeCottoni_model extends CI_Model {
	public function __construct(){
		$this->load->database();
		$this->load->library('blo_form');
	}

	public function searchVolume($form){
		$this->blo_form->init($form);

		$this->db->select('SUM(aa.volumeWeight) AS TOTAL');
		$this->db->from('tb_volume_cottoni AS aa');
		$this->db->join('tb_user_profile AS bb','aa.volumeUserId = bb.profilUserId');
		foreach($form as $key=>$val){
			$search = $this->blo_form->get_form_value($key);

			if($search != ''){
				$this->db->where($val['column_name'].' = ',$search);
			}
		}

		return $this->db->get()->result_array()[0]["TOTAL"];
	}

	public function searchVolumeByArea($form,$onlyArea=false){
		$this->blo_form->init($form);

		$this->db->select('SUM(aa.volumeWeight) AS TOTAL');
		$this->db->from('tb_volume_cottoni AS aa');
		$this->db->join('tb_user_profile AS bb','aa.volumeUserId = bb.profilUserId');
		$this->db->where('bb.profilType IN (SELECT usrconPenjualId AS id FROM tb_user_connection WHERE usrconUserId = '.$_SESSION['type'].') ');
		foreach($form as $key=>$val){
			$search = $this->blo_form->get_form_value($key);

			if($search != '' && !$onlyArea){
				$this->db->where($val['column_name'].' = ',$search);
			}
			else if($onlyArea){
				if($search != '' && strpos($key, 'profil_'))
					$this->db->where($val['column_name'].' = ',$search);
			}
		}

		return $this->db->get()->result_array()[0]["TOTAL"];
	}

	public function searchUserForVolume($form){
		$this->blo_form->init($form);

		$this->db->select('bb.profilNama,bb.profilAlamat,cc.profTypeName,aa.volumeTimestamp,aa.volumeWaste,aa.volumeKadarAir,aa.volumeBeanCount,aa.volumeTersertifikasi,aa.volumeWeight,aa.volumeId,aa.volumeUserId AS userId');
		$this->db->from('tb_volume_cottoni AS aa');
		$this->db->join('tb_user_profile AS bb','aa.volumeUserId = bb.profilUserId');
		$this->db->join('tb_profil_type AS cc','bb.profilType = cc.profTypeId');
		$this->db->where('bb.profilType IN (SELECT usrconPenjualId AS id FROM tb_user_connection WHERE usrconUserId = '.$_SESSION['type'].') ');

		foreach($form as $key=>$val){
			$search = $this->blo_form->get_form_value($key);

			if($search != ''){
				$this->db->where($val['column_name'].' = ',$search);
			}
		}

		return $this->db->get()->result_array();
	}

	public function pembelian($data=null){
		if($data == null){
			return false;
		}

		$this->db->select();
		$this->db->from('tb_volume_cottoni');
		$this->db->where('volumeId = ',$data['volumeId']);
		$details = $this->db->get()->result_array()[0];

		$details['volumeDibeli'] = $data['volumeDibeli'];
		$details['pembeliId'] = $_SESSION['id'];

		if($details['volumeDibeli']>$details['volumeWeight']){
			return false;
		}

		$insertBeli = array(
			'beliVolumeId' => $details['volumeId'],
			'beliUserId' => $details['pembeliId'],
			'beliTimestamp' => time(),
			'beliWaste' => $details['volumeWaste'],
			'beliKadarAir' => $details['volumeKadarAir'],
			'beliBeanCount' => $details['volumeBeanCount'],
			'beliTersertifikasi' => $details['volumeTersertifikasi'],
			'beliVolume' => $details['volumeDibeli']
		);

		//~ $sql = $this->db->set($insertBeli)->get_compiled_insert('tb_beli');
		$sql = $this->db->set($insertBeli)->insert('tb_beli');
		console_html($sql);

		$updateVol = array(
			'volumeWeight' => ($details['volumeWeight']-$details['volumeDibeli'])
		);

		$this->db->where('volumeId = ',$details['volumeId']);
		//~ $sql = $this->db->set($updateVol)->get_compiled_update('tb_volume_cottoni');
		$sql = $this->db->set($updateVol)->update('tb_volume_cottoni');
		console_html($sql);


		$insertNewVolume = array(
			'volumeUserId' => $details['pembeliId'],
			'volumeTimestamp' => time(),
			'volumeWaste' => $details['volumeWaste'],
			'volumeKadarAir' => $details['volumeKadarAir'],
			'volumeBeanCount' => $details['volumeBeanCount'],
			'volumeTersertifikasi' => $details['volumeTersertifikasi'],
			'volumeWeight' => $details['volumeDibeli']
		);

		//~ $sql = $this->db->set($insertNewVolume)->get_compiled_insert('tb_beli');
		$sql = $this->db->set($insertNewVolume)->insert('tb_volume_cottoni');

		return true;
	}
}
