<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class login_model extends CI_Model {
	protected $username;
	protected $password;
	
	public function __construct(){
		$this->load->database();
	}
	
	public function logging_in($username,$password){
		$this->username = $username;
		$this->password = sha1($password);
		
		$this->db->from("tb_user")
				->where('username',$this->username)
				->where('password',$this->password );
		
		$result = $this->db->count_all_results();
		
		$this->set_session($result);
		return $result;
	}
		
	public function generate_sidebar($type){
		$this->db->select('
						className AS name,
						classShownName AS title,
						classSidebarIcon AS icon,
						clgrName AS group
						');
		$this->db->from("tb_classes");
		$this->db->join("tb_classes_group",'classGroup=clgrId');
		$this->db->where('classSpecial = ',$type);
		$this->db->where('classOrder IS NOT NULL');
		$this->db->or_where('classIsGlobal = 1');

		$this->db->order_by('classOrder ASC,classId ASC');
		//~ echo $this->db->get_compiled_select("tb_classes");
		$_SESSION['sidebar'] = $this->db->get()->result_array();
		
		return;
	}
	
	protected function set_session($is_valid=FALSE){
		if(!$is_valid){
			return;
		}
			
		$session_items = $this->db->select('
										aa.id AS id,
										aa.username AS username,
										bb.profilType AS type
										'
									)
								->from('tb_user AS aa')
								->join('tb_user_profile AS bb','aa.id=bb.profilUserId','left')
								->where('aa.username = ',$this->username)
								->get()
								->row();
									
		$this->session->set_userdata('logged_in', time());
		$this->session->set_userdata('id', $session_items->id );
		$this->session->set_userdata('username', $session_items->username );
		$this->session->set_userdata('type', $session_items->type );
		
		$this->generate_sidebar($session_items->type);
	}
}
