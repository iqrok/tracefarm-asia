<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class profileSpinosum_model extends CI_Model {
	protected $form;

	public function __construct(){
		$this->load->database();
		$this->load->library('blo_form');
	}

	public function set_form($form,$data=null,$fromDB=false){
		$this->blo_form->init($form);

		if($data!=null)
			$this->blo_form->update_form($data,$fromDB);

		$this->form = $this->blo_form->get_form();
		return;
	}

	public function get_form(){
		return $this->form;
	}

	public function insert_update_db($table,$data,$action,$where='profilUserId'){
		$action = strtolower($action);

		foreach($this->form as $key=>$val){
			if(isset($data[$key])){
				$this->db->set($val['column_name'],$data[$key]);
			}
			else if($val['type']=='data'){
				$this->db->set($val['column_name'],$val['attr']['value']);
			}
		}

		switch($action){
			case 'insert':
				$this->db->insert($table);
				break;

			case 'update':
				$this->db->where($where,$_SESSION['id']);
				$this->db->update($table);
				break;
		}
	}

	public function check_exist($id,$table='tb_user_profile',$where='profilUserId'){
		$this->db->select('*');
		$this->db->from($table);
		$this->db->where($where.'=',$id);
		return $this->db->count_all_results();
	}

	public function get_profile_raw($userId){
		if(!$this->check_exist($userId))
			return false;

		$this->db->select(' profilUserId,
							profilType,
							profilNama,
							profilEmail,
							profilAlamat,
							profilJmlAnggotaKeluarga,
							profilHasCert,
							profilIsOrganic,
							profilKelompok ,
							profilProvinsi ,
							profilKabupaten ,
							profilKecamatan ,
							profilDesa ,
							profilPrediksiTahunDepan ,
							profilPrediksiTahunLalu
						');
		$this->db->from('tb_user_profile');
		$this->db->where('profilUserId=',$userId);
		//~ console_html($this->db->get()->result_array());exit;
		return $this->db->get()->result_array()[0];
	}

	public function get_profile($userId){
		$data = $this->get_profile_raw($userId);

		if($data === false)
			return false;

		$this->set_form($this->form,$data,true);

		return $this->get_form();
	}

	public function save_profile($userId,$form,$data){
		$this->set_form($form,$data);

		if($this->check_exist($userId))
			$action = 'update';
		else
			$action = 'insert';

		$this->insert_update_db("tb_user_profile",$data,$action);

		$this->update_session();
		return;
	}

	public function save_kebun($userId,$form,$data){
		$this->set_form($form,$data);

		$action = 'insert';

		$this->insert_update_db("tb_kebun",$data,$action,'kebunUserId');

		$this->update_session();
		return;
	}

	public function save_volume($userId,$form,$data){
		$this->set_form($form,$data);

		$action = 'insert';

		$this->insert_update_db("tb_volume_spinosum",$data,$action,'volumeUserId');

		$this->update_session();
		return;
	}

	public function delete_kebun($kebunId){
		$this->db->where('kebunId', $kebunId);
		$this->db->delete('tb_kebun');

		return;
	}

	protected function update_session(){
		$session_items = $this->db->select('
										aa.id AS id,
										aa.username AS username,
										bb.profilType AS type
										'
									)
								->from('tb_user AS aa')
								->join('tb_user_profile AS bb','aa.id=bb.profilUserId','left')
								->where('aa.id = ',$_SESSION['id'])
								->get()
								->row();

		$this->session->set_userdata('type', $session_items->type );
		return;
	}

	public function listKebun($userId){
		$this->db->select('*');
		$this->db->from('tb_kebun');
		$this->db->where('kebunUserId=',$userId);
		$this->db->order_by('kebunId ASC');
		return $this->db->get()->result_array();
	}

	public function listVolume($userId){
		$this->db->select('*');
		$this->db->from('tb_volume_spinosum');
		$this->db->where('volumeUserId=',$userId);
		$this->db->order_by('volumeId DESC');
		return $this->db->get()->result_array();
	}

	public function getProvinces(){
		$this->db->select('provinceName AS name');
		$this->db->from('ref_provinces');
		$this->db->order_by('provinceName ASC');
		$result = $this->db->get()->result_array();

		$data = array(''=>'-');
		foreach($result as $value){
			$data[$value['name']] = $value['name'];
		}

		return $data;
	}

	public function getRegencies($province){
		$this->db->select('regencyName AS name,regencyId AS id');
		$this->db->from('ref_regencies AS aa');
		$this->db->join('ref_provinces AS bb','aa.provinceId = bb.provinceId');
		$this->db->where('bb.provinceName =',$province);
		$this->db->order_by('regencyName ASC');
		$result = $this->db->get()->result_array();

		$data = array(''=>'[PILIH SALAH SATU]');
		foreach($result as $value){
			$data[$value['name']] = $value['name'];
		}

		return $data;
	}

	public function getDistricts($province,$regency){
		$this->db->select('cc.districtName AS name');
		$this->db->from('ref_districts AS cc');
		$this->db->join('ref_regencies AS aa','aa.regencyId = cc.regencyId');
		$this->db->join('ref_provinces AS bb','aa.provinceId = bb.provinceId');
		$this->db->where('bb.provinceName =',$province);
		$this->db->where('aa.regencyName =',$regency);
		$this->db->order_by('districtName ASC');
		$result = $this->db->get()->result_array();

		$data = array(''=>'[PILIH SALAH SATU]');
		foreach($result as $value){
			$data[$value['name']] = $value['name'];
		}

		return $data;
	}

	public function getVillages($province,$regency,$district){
		$this->db->select('dd.villageName AS name');
		$this->db->from('ref_villages AS dd');
		$this->db->join('ref_districts AS cc','dd.districtId = cc.districtId');
		$this->db->join('ref_regencies AS aa','aa.regencyId = cc.regencyId');
		$this->db->join('ref_provinces AS bb','aa.provinceId = bb.provinceId');
		$this->db->where('cc.districtName =',$district);
		$this->db->where('bb.provinceName =',$province);
		$this->db->where('aa.regencyName =',$regency);
		$this->db->order_by('villageName ASC');
		$result = $this->db->get()->result_array();
		//~ echo $this->db->get_compiled_select();
		$data = array(''=>'[PILIH SALAH SATU]');
		foreach($result as $value){
			$data[$value['name']] = $value['name'];
		}

		return $data;
	}
}
