<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class dataVolume_model extends CI_Model {	
	public function __construct(){
		$this->load->database();
		$this->load->library('blo_form');
	}

	public function searchVolume($form){
		$this->blo_form->init($form);
		
		$this->db->select('SUM(aa.volumeWeight) AS TOTAL');
		$this->db->from('tb_volume AS aa');
		$this->db->join('tb_user_profile AS bb','aa.volumeUserId = bb.profilUserId');
		foreach($form as $key=>$val){
			$search = $this->blo_form->get_form_value($key);

			if($search != ''){
				$this->db->where($val['column_name'].' = ',$search);
			}
		}
		
		return $this->db->get()->result_array()[0]["TOTAL"];
	}

	public function searchVolumeByArea($form,$onlyArea=false){
		$this->blo_form->init($form);
		
		$this->db->select('SUM(aa.volumeWeight) AS TOTAL');
		$this->db->from('tb_volume AS aa');
		$this->db->join('tb_user_profile AS bb','aa.volumeUserId = bb.profilUserId');
		//$this->db->where('bb.profilType IN (SELECT usrconPenjualId AS id FROM tb_user_connection WHERE usrconUserId = '.$_SESSION['type'].') ');
		foreach($form as $key=>$val){
			$search = $this->blo_form->get_form_value($key);

			if($search != '' && !$onlyArea){
				$this->db->where($val['column_name'].' = ',$search);
			}
			else if($onlyArea){
				if($search != '' && strpos($key, 'profil_'))
					$this->db->where($val['column_name'].' = ',$search);
			}
		}
		$total = $this->db->get()->result_array()[0]["TOTAL"];
		
		return $total;
	}

	public function searchUserForVolume($form){
		$this->blo_form->init($form);
		
		$this->db->select('bb.profilNama,bb.profilAlamat,cc.profTypeName,aa.volumeTimestamp,aa.volumeWaste,aa.volumeKadarAir,aa.volumeBeanCount,aa.volumeTersertifikasi,aa.volumeWeight,aa.volumeId,aa.volumeUserId AS userId,dd.username AS ID');
		$this->db->from('tb_volume AS aa');
		$this->db->join('tb_user_profile AS bb','aa.volumeUserId = bb.profilUserId');
		$this->db->join('tb_profil_type AS cc','bb.profilType = cc.profTypeId');
		$this->db->join('tb_user AS dd','bb.profilUserId = dd.id');
		//$this->db->where('bb.profilType IN (SELECT usrconPenjualId AS id FROM tb_user_connection WHERE usrconUserId = '.$_SESSION['type'].') ');
		
		foreach($form as $key=>$val){
			$search = $this->blo_form->get_form_value($key);

			if($key == 'profil_Provinsi' || $key == 'profil_Kabupaten' || $key == 'profil_Kecamatan' || $key == 'profil_Kelurahan'){
				if($search == '[PILIH SALAH SATU]')
					$search='';
			}

			if($key == 'find_kelompok'){
				$search = '';
			}
			
			if($search != ''){
				$this->db->where($val['column_name'].' = ',$search);
			}
		}
		
		$find_kelompok = $this->blo_form->get_form_value('find_kelompok');
		if($find_kelompok != ''){
			if(strpos($find_kelompok,',')>=0)
				$find_kelompok = explode(',',$find_kelompok);
			else
				$find_kelompok = array($find_kelompok);

			$queryTmp = '';
			$i=0;
			foreach($find_kelompok as $val){
				$queryTmp .= (($i++>0?' OR ':'').'bb.profilType = '.$val);
			};
			$this->db->where('('.$queryTmp.')');
		}
		
		//~ echo $this->db->get_compiled_select('',false);
		return $this->db->get()->result_array();
	}

	public function pembelian($data=null){
		if($data == null){
			return false;
		}

		$this->db->select(); 
		$this->db->from('tb_volume'); 
		$this->db->where('volumeId = ',$data['volumeId']);
		$details = $this->db->get()->result_array()[0];
		
		$details['volumeDibeli'] = $data['volumeDibeli'];
		$details['pembeliId'] = $_SESSION['id'];

		if($details['volumeDibeli']>$details['volumeWeight']){
			return false;
		}

		$insertBeli = array(
			'beliVolumeId' => $details['volumeId'],
			'beliUserId' => $details['pembeliId'],
			'beliTimestamp' => time(),
			'beliWaste' => $details['volumeWaste'],
			'beliKadarAir' => $details['volumeKadarAir'],
			'beliBeanCount' => $details['volumeBeanCount'],
			'beliTersertifikasi' => $details['volumeTersertifikasi'],
			'beliVolume' => $details['volumeDibeli']
		);

		//~ $sql = $this->db->set($insertBeli)->get_compiled_insert('tb_beli');
		$sql = $this->db->set($insertBeli)->insert('tb_beli');
		//console_html($sql);
		
		$updateVol = array(
			'volumeWeight' => ($details['volumeWeight']-$details['volumeDibeli'])
		);

		$this->db->where('volumeId = ',$details['volumeId']);
		//~ $sql = $this->db->set($updateVol)->get_compiled_update('tb_volume');
		$sql = $this->db->set($updateVol)->update('tb_volume');
		//console_html($sql);

		
		$insertNewVolume = array(
			'volumeUserId' => $details['pembeliId'],
			'volumeTimestamp' => time(),
			'volumeWaste' => $details['volumeWaste'],
			'volumeKadarAir' => $details['volumeKadarAir'],
			'volumeBeanCount' => $details['volumeBeanCount'],
			'volumeTersertifikasi' => $details['volumeTersertifikasi'],
			'volumeWeight' => $details['volumeDibeli']
		);
		
		//~ $sql = $this->db->set($insertNewVolume)->get_compiled_insert('tb_beli');
		$sql = $this->db->set($insertNewVolume)->insert('tb_volume');
		
		return true;
	}
}
