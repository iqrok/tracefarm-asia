<?php
class session_checking{
	public function session_checking_redirect()
	{
		// load the instance
		$this->CI =& get_instance();
		
		if (!isset($this->CI->session)){
			$this->CI->load->library('session');
		}
		$controller = strtolower($this->CI->router->class);
		$method = $this->CI->router->method;
		
		if(!$this->CI->session->userdata('logged_in') && ($controller!='login' && $controller!='register'))
			redirect(site_url('login'));

		if(isset($_SESSION['id'])){
			$this->CI->load->model('profile_model');
	    	$this->CI->load->model('login_model');
	    	
	    	$this->CI->login_model->generate_sidebar($_SESSION['type']);

			if(!$this->CI->profile_model->check_exist($_SESSION['id']) && $controller!='login'){
				$this->CI->session->set_flashdata('alert', 'Harap melengkapi Profile');

				if($controller!='profile')
					redirect(site_url('profile'));
			}
		}
		return;
	}
}
