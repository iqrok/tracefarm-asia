<?php defined('BASEPATH') OR exit('No direct script access allowed');?>

<div class="masonry-sizer col-md-12"></div>
<div class="masonry-item col-md-12 mT-30">
	<div class="bgc-white p-20 bd">
		<h4>Daftar Petani</h4>
		<div id="dataTable_wrapper" class="dataTable_wrapper">
			<table id="dataTable" class="table table-striped table-bordered dataTable" role="grid" aria-describedby="dataTable_info" style="width: 100%;" width="100%" cellspacing="0">
				<thead>
					<tr>
						<th>No</th>
						<th>Nama</th>
						<th>Alamat</th>
						<th>Email</th>
						<th>Kelompok Tani</th>
						<th>Hasil Setahun Lalu (Ton)</th>
						<th>Prediksi Tahun Depan (Ton)</th>
					</tr>
				</thead>
				<tbody>
				<?php
					$i=1;
					foreach($table as $key=>$val){ ?>
					<tr>
						<td><?php echo $i++; ?></td>
						<td><?php echo $val['profilNama']; ?></td>
						<td><?php echo $val['profilAlamat']; ?></td>
						<td><?php echo $val['profilEmail']; ?></td>
						<td><?php echo $val['profilKelompok']; ?></td>
						<td><?php echo $val['profilPrediksiTahunLalu']; ?></td>
						<td><?php echo $val['profilPrediksiTahunDepan']; ?></td>
					</tr>
				<?php }	?>
				</tbody>
			</table>
		</div>
	</div>
</div>
