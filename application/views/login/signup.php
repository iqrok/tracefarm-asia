<?php defined('BASEPATH') OR exit('No direct script access allowed');?><!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Sign Up</title>
    <style>
      #loader {
        transition: all 0.3s ease-in-out;
        opacity: 1;
        visibility: visible;
        position: fixed;
        height: 100vh;
        width: 100%;
        background: #fff;
        z-index: 90000;
      }

      #loader.fadeOut {
        opacity: 0;
        visibility: hidden;
      }

      .spinner {
        width: 40px;
        height: 40px;
        position: absolute;
        top: calc(50% - 20px);
        left: calc(50% - 20px);
        background-color: #333;
        border-radius: 100%;
        -webkit-animation: sk-scaleout 1.0s infinite ease-in-out;
        animation: sk-scaleout 1.0s infinite ease-in-out;
      }

      @-webkit-keyframes sk-scaleout {
        0% { -webkit-transform: scale(0) }
        100% {
          -webkit-transform: scale(1.0);
          opacity: 0;
        }
      }

      @keyframes sk-scaleout {
        0% {
          -webkit-transform: scale(0);
          transform: scale(0);
        } 100% {
          -webkit-transform: scale(1.0);
          transform: scale(1.0);
          opacity: 0;
        }
      }
    </style>
    <link rel="icon" type="image/png" href="<?php echo base_url()."assets"; ?>/images/logo1.png" />
  </head>
  <body class="app">
    <div id='loader'>
      <div class="spinner"></div>
    </div>

    <script>
      window.addEventListener('load', () => {
        const loader = document.getElementById('loader');
        setTimeout(() => {
          loader.classList.add('fadeOut');
        }, 300);
      });
    </script>
    <div class="peers ai-s fxw-nw h-100vh">
	<?php if(time()%2){ ?>	
      <div class="peer peer-greed h-100 pos-r bgr-n bgpX-c bgpY-c bgsz-cv" style='background-color: #fff'>
		  <div style='width:90%; border:0px solid red;margin:auto; margin-top:2rem; padding:10px;'>
			<img src="<?php echo base_url()."assets"; ?>/images/loginImg1.png" style="width:100%;">
		  </div>
		  <div style='width:90%; border:0px solid red;margin:auto; margin-top:2.5rem; padding:10px;'>
			<img src="<?php echo base_url()."assets"; ?>/images/loginImg2a.png" style="width:100%;">
		  </div>
      </div>
      <?php }else{ ?>
      <div class="peer peer-greed h-100 pos-r bgr-n bgpX-c bgpY-c bgsz-cv" style='background-color: #fff'>
		  <div style='width:100%; border:0px solid red;margin:auto; margin-top:3rem;'>
			<img src="<?php echo base_url()."assets"; ?>/images/loginImg3a.png" style="width:100%;">
		  </div>
      </div>
	<?php } ?>
      <div class="col-12 col-md-4 peer pX-40 pY-80 h-100 bgc-white scrollable pos-r" style='min-width: 320px;'>
		<div class="row">
			<div class="col-12" style='height: 120px;'>
				<div class="pos-a centerXY">
				  <div class="bgc-white bdrs-50p pos-r" style='width: 120px; height: 120px;'>
					<img class="pos-a centerXY" style="width:128px;" src="<?php echo base_url()."assets"; ?>/images/logo1.png" alt="">
				  </div>
				</div>
			</div>
		</div>
        <h4 class="fw-300 c-grey-900 mB-40">Register</h4>
        <?php if(isset($error)):?>
        <div class="alert alert-danger" role="alert"><?php echo $error;?></div>
        <?php endif;?>
        <form method=POST action="<?php echo site_url('register'); ?>">
		<?php
			foreach($form as $key=>$value){
				echo '<div class="form-group">';
				echo print_form($value);
				echo '</div>';
			}

			echo '<div class="form-group">';
			echo '<input type=submit class="btn btn-primary" type="button" value="'.$button_text.'">';
			echo '</div>';
		?>
        </form>
        <div>
			<a href='<?php echo site_url('login');?>'>Halaman Login</a>
        </div>
        <div style="text-align:center;width:50%;margin:auto;margin-top:2rem">
			<h4 style="font-size:1.5rem">Ikuti Sosial Media Kami</h4>
			<div class="row">
				<div class="col-3">
					<a href='https://facebook.com/TraceFarmID' target="_blank" style="color:#0044C1; font-size:2.5em">
						<i class="fa fa-facebook-square fa-4" aria-hidden="true"></i>
					</a>
				</div>
				<div class="col-3">						
					<a href='https://instagram.com/tracefarmid' target="_blank" style="color:#D94FFB; font-size:2.5em">
						<i class="fa fa-instagram fa-4" aria-hidden="true"></i>
					</a>					
				</div>
				<div class="col-3">					
					<a href='https://twitter.com/tracefarm' target="_blank" style="color:#33A4FF; font-size:2.5em">
						<i class="fa fa-twitter-square fa-4" aria-hidden="true"></i>
					</a>					
				</div>
				<div class="col-3">					
					<a href='http://wa.me/628114443738' target="_blank" style="color:#07DA0D; font-size:2.5em">
						<i class="fa fa-whatsapp fa-4" aria-hidden="true"></i>
					</a>										
				</div>
				
			</div>
        </div>
      </div>
    </div>

    <script type="text/javascript" src="<?php echo base_url()."assets/adminator/"; ?>vendor.js"></script>
    <script type="text/javascript" src="<?php echo base_url()."assets/adminator/"; ?>bundle.js"></script>
  </body>
</html>
