<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="masonry-sizer col-md-12"></div>

<div class="masonry-item col-md-12 mT-30 w-100">
	<div class="bgc-white p-20 bd">
		<h3 class="c-grey-900"><?php echo $title; ?></h3>
		<div class="mT-30">
			<form method=POST action="<?php echo site_url('dataVolumeGracilaria/searchVolume'); ?>">
			<?php
				foreach($form as $key=>$value){
					echo '<div class="form-group row">';
					echo print_form($value,true);
					echo '</div>';
				}
			?>
				<div class="form-group">
					<input type=submit class="btn btn-primary" type="button" value="Cari">
				</div>
			</form>
		</div>
		<div class="mT-30">
			<div class="row">
				<h3 class="col-3 text-dark text-right">Volume Tersedia</h3>
				<h3 class="col-3 text-primary text-left"><?php echo (isset($volume) && $volume!=NULL)?($volume.' Ton'):('-'); ?></h3>
			</div>
		</div>
	</div>
</div>
