<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="masonry-sizer col-md-12"></div>

<div class="masonry-item col-md-12 mT-30 w-100">
	<div class="bgc-white p-20 bd">				
		<h3 class="c-grey-900"><?php echo $title; ?></h3>
		<div class="mT-30">					
			<form method=POST action="<?php echo site_url('dataVolumeGracilaria/searchLogistik'); ?>">
			<?php
				foreach($form as $key=>$value){
					echo '<div class="form-group row">';
					echo print_form($value,true);
					echo '</div>';
				}					
			?>
				<div class="form-group">
					<input type=submit class="btn btn-primary" type="button" value="Cari">
				</div>
			</form>
		</div>
		
		<div class="mT-30">
			<div class="row">
				<h1 class="col-12 text-center text-info"><?php echo (isset($desa)?($desa.', '):('')).(isset($kecamatan)?($kecamatan.', '):('')).(isset($kabupaten)?($kabupaten.', '):('')).(isset($provinsi)?($provinsi):(''))?></h1>
			</div>
			<div class="row">
				<h4 class="col-12 text-center">Filter : <?php
					$color = ['muted','success','danger','info'];
					if(isset($filter)){
						$i=0;
						foreach($filter as $key=>$val){
							echo "<span class='text-".$color[$i++]."'>".$key.":".$val." </span>";
						};
					}
				?></h4>
			</div>
			<div class="row">
				<h3 class="col-6 text-dark text-right">Total Volume Tersedia di Area</h3>
				<h3 class="col-6 text-primary text-left"><?php echo (isset($volume_area) && $volume_area!=NULL)?($volume_area.' Ton'):('-'); ?></h3>
			</div>
			<div class="row">
				<h3 class="col-6 text-dark text-right">Volume Tersedia Terfilter</h3>
				<h3 class="col-6 text-primary text-left"><?php echo (isset($volume) && $volume!=NULL)?($volume.' Ton'):('-'); ?></h3>
			</div>
		</div>
		
		<div class="mT-30">
			<table class="table table-hover">
				<tr>
					<th><div style="text-align:center">No</div></th>
					<th><div style="text-align:center">Nama Pemilik</div></th>
					<th><div style="text-align:center"></div></th>
					<th><div style="text-align:center">Alamat</div></th>
					<th><div style="text-align:center">Tanggal Pencatatan</div></th>
					<th><div style="text-align:center">Waste (%)</div></th>
					<th><div style="text-align:center">Kadar Air (%)</div></th>
					<th><div style="text-align:center">Warna</div></th>
					<th><div style="text-align:center">Volume (ton)</div></th>
					<th><div style="text-align:center">Aksi</div></th>
				</tr>
			<?php
				$i=1;
				foreach($table as $key=>$val){
					echo '<tr>
						<td><div style="text-align:center">'.$i++.'</div></td>
						<td><div style="text-align:center">'.$val['profilNama'].'</div></td>
						<td><div style="text-align:center">'.$val['profTypeName'].'</div></td>
						<td><div style="text-align:center">'.$val['profilAlamat'].'</div></td>
						<td><div style="text-align:center">'.convert_date($val['volumeTimestamp']).'</div></td>
						<td><div style="text-align:center">'.$val['volumeWaste'].'%</div></td>
						<td><div style="text-align:center">'.$val['volumeKadarAir'].'%</div></td>
						<td><div style="text-align:center">Grade <b>'.$val['volumeBeanCount'].'</b></div></td>
						<td><div style="text-align:center" class="volumeNow" data-volume="'.$val['volumeWeight'].'">'.$val['volumeWeight'].' ton</div></td>
						<td>
							<div style="text-align:center">
								<form class="form-inline" method="POST" action="'.site_url('dataVolumeGracilaria/pembelian').'">			
									<div class="form-group mb-2">
										<input type="text" class="form-control numOnly" name="volumeDibeli" id="volumeDibeli" placeholder="Volume Dibeli" required>
										<input type=hidden name=volumeId value="'.$val['volumeId'].'">
										<input type=hidden name=userId value="'.$val['userId'].'">
									</div>
									<input type=submit class="btn btn-success ml-1 mb-2" value=Beli>
								</form>
							</div>
						</td>
					</tr>';
				}					
			?>
			</table>
		</div>
	</div>
</div>
