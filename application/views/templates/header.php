<?php 
	defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=yes">
    <link rel="icon" type="image/png" href="<?php echo base_url()."assets"; ?>/images/logo1.png" />
    <title>Tracefarm</title>
    <style>#loader{transition:all .3s ease-in-out;opacity:1;visibility:visible;position:fixed;height:100vh;width:100%;background:#fff;z-index:90000}#loader.fadeOut{opacity:0;visibility:hidden}.spinner{width:40px;height:40px;position:absolute;top:calc(50% - 20px);left:calc(50% - 20px);background-color:#333;border-radius:100%;-webkit-animation:sk-scaleout 1s infinite ease-in-out;animation:sk-scaleout 1s infinite ease-in-out}@-webkit-keyframes sk-scaleout{0%{-webkit-transform:scale(0)}100%{-webkit-transform:scale(1);opacity:0}}@keyframes sk-scaleout{0%{-webkit-transform:scale(0);transform:scale(0)}100%{-webkit-transform:scale(1);transform:scale(1);opacity:0}}</style>
    <link href="<?php echo base_url()."assets/adminator/"; ?>style.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
	<script src="<?php echo base_url()."assets"; ?>/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/w/dt/jszip-2.5.0/dt-1.10.18/af-2.3.3/b-1.5.6/b-flash-1.5.6/b-html5-1.5.6/b-print-1.5.6/cr-1.5.0/fc-3.2.5/fh-3.1.4/kt-2.5.0/r-2.2.2/datatables.min.css"/>
 
	<script src="<?php echo base_url()."assets"; ?>/js/html_to_pdfmake.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/pdfmake@0.1.57/build/pdfmake.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/pdfmake@0.1.57/build/vfs_fonts.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/w/dt/jszip-2.5.0/dt-1.10.18/af-2.3.3/b-1.5.6/b-flash-1.5.6/b-html5-1.5.6/b-print-1.5.6/cr-1.5.0/fc-3.2.5/fh-3.1.4/kt-2.5.0/r-2.2.2/datatables.min.js"></script>
	
	
	<?php if(isset($chart)): 
	switch(strtolower($chart)){
		case 'highcharts' : 
		?>
		<!-- highcharts -->
		<link rel="stylesheet" href="<?php echo base_url()."assets"; ?>/highcharts/css/highcharts.css">
		<script src="<?php echo base_url()."assets"; ?>/highcharts/highcharts.js"></script>
		<script src="<?php echo base_url()."assets"; ?>/highcharts/modules/exporting.js"></script>
		<script src="<?php echo base_url()."assets"; ?>/highcharts/modules/export-data.js"></script>
	<?php	
		break;
	}
	?>
	<?php endif; ?>
	
    <script>window.addEventListener('load', () => {
      const loader = document.getElementById('loader');
      setTimeout(() => {
        loader.classList.add('fadeOut');
      }, 300);
      });
      
		$(document).ready(function () {
			$('.datepicker').datepicker({
				autoclose: true,
				orientation: 'bottom',
				todayHighlight: true,
				zIndexOffset:999,
				format: "dd-mm-yyyy"
			});
		});
      
	
		$(document).ready(function(){
			$('.datepicker_month').datepicker( {
				format: 'mm-yyyy',
				viewMode: 'months', 
				orientation: 'bottom',
				minViewMode: 'months'
			});
		});

		$(document).ready(function(){
			$('.numOnly').keyup(function(e){
				var ini = $(this);
				ini.val(ini.val().replace(/[^0-9.,]+/g,''));
				ini.val(ini.val().replace(/[,]+/g,'.'));
			});
		});
		
		$(document).ready(function () {
			$('.__delete-item').click(function(){
				var ini = $(this);
				ini.hide();
				ini.siblings().fadeIn('slow');
			});
		});

		$(document).ready(function () {
			$('.__cancel-delete').click(function(){
				var ini = $(this).parents('.__delete-confirm');
				ini.hide();
				ini.siblings().fadeIn('slow');
			});
		});
		
		$(document).ready(function(){
			var d = new Date();
			var dStr = d.getFullYear()+'-'+(d.getMonth()+1)+'-'+d.getDate(); 
			$('.__dataTable__').DataTable({
				aLengthMenu: [
					[25, 50, 100, 200, -1],
					[25, 50, 100, 200, "All"]
				],
				iDisplayLength: 25,
				dom: 'lfrtBip',
				buttons: [
					{
						extend: 'excelHtml5',
						title: '[Excel] '+$('.c-grey-900').text()+' '+dStr
					},
					{
						extend: 'pdfHtml5',
						title: '[PDF] '+$('.c-grey-900').text()+' '+dStr
					},
					'print'
				]
			});
		});
    </script>
  </head>
  <Body class="app">
    <div id="loader">
      <div class="spinner"></div>
    </div>
    <div id="main-container">
