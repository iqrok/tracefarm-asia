<?php 
	defined('BASEPATH') OR exit('No direct script access allowed');
	
?>
      <div class="sidebar">
        <div class="sidebar-inner">
          <div class="sidebar-logo">
            <div class="peers ai-c fxw-nw">
              <div class="peer peer-greed">
                <a class="sidebar-link td-n" href="index.html">
                  <div class="peers ai-c fxw-nw">
                    <div class="peer">
                      <div class="logo"><img style="width:50px;margin-top:5px;"  src="<?php echo base_url()."assets"; ?>/images/logo1.png" alt=""></div>
                    </div>
                    <div class="peer peer-greed">
                      <h5 class="lh-1 mB-0 logo-text">Tracefarm</h5>
                    </div>
                  </div>
                </a>
              </div>
              <div class="peer">
                <div class="mobile-toggle sidebar-toggle"><a href="" class="td-n"><i class="ti-arrow-circle-left"></i></a></div>
              </div>
            </div>
          </div>
          <ul class="sidebar-menu scrollable pos-r">
			<li class="nav-item mT-30 active"></li>
			<!-- Sidebar's Items -->
			<?php
				$sidebar = array();
				$i=0;
				foreach($_SESSION['sidebar'] as $key => $val) {
					$sidebar[$val['group']][$i]['name'] = $val['name'];
					$sidebar[$val['group']][$i]['icon'] = $val['icon'];
					$sidebar[$val['group']][$i]['title'] = $val['title'];
					$i++;
				}
				//~ console_html($sidebar);
			?>
			<?php foreach($sidebar as $key => $val) {?>
			<li id="<?php echo $key; ?>" class="nav-item dropdown open">
				<a class="dropdown-toggle" href='javascript:void(0);'>
					<span class="icon-holder">
						<i class="c-blue-500 ti-arrow-circle-right"></i>
					</span>
					<span class="title"><b><?php echo $key ?></b></span>
				</a>
				<ul class="dropdown-menu">
				<?php foreach($val as $val1){
					echo 
						'<li class="nav-item">
							<a class="sidebar-link" href='.site_url($val1['name']).'>
								<span class="icon-holder">
									<i class="c-blue-500 '.$val1['icon'].'"></i>
								</span>
								<span class="title">'.$val1['title'].'</span>
							</a>
						</li>';
				 } ?>
				</ul>
			</li>
			<?php } ?>
			<!-- END OF Sidebar's Items -->
			
          </ul>
        </div>
      </div> 
