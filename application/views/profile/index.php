<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="masonry-sizer col-md-6"></div>

<div class="masonry-item w-100" style="position: absolute; left: 0%; top: 0px;">
	<div class="row gap-20">
		<div class="col-md-6">
			<a href='<?php echo site_url('databaseKebun/biodata'); ?>'>
				<div class="layers bd bgc-white p-20">
					<div class="layer w-100 mB-10">
						<h4 class="lh-1">Biodata</h4>
					</div>
				</div>
			</a>
		</div>
		
		<div class="col-md-6">
			<a href='<?php echo site_url('databaseKebun/dataKebun'); ?>'>
				<div class="layers bd bgc-white p-20">
					<div class="layer w-100 mB-10">
						<h4 class="lh-1">Data Kebun</h4>
					</div>
				</div>
			</a>
		</div>
		
		<div class="col-md-6">
			<a href='<?php echo site_url('databaseKebun/dataVolumeKakao'); ?>'>
				<div class="layers bd bgc-white p-20">
					<div class="layer w-100 mB-10">
						<h4 class="lh-1">Data Volume Biji Kakao yang Dimiliki</h4>
					</div>
				</div>
			</a>
		</div>
	</div>
</div>
