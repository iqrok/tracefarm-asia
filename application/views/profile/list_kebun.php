<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="masonry-sizer col-md-6"></div>

<div class="masonry-item col-md-6 mT-30 w-100">
	<div class="bgc-white p-20 bd">				
		<h3 class="c-grey-900"><?php echo $title; ?></h3>
		<div class="mT-30">					
			<form method=POST action="<?php echo ($_SESSION['type']==7)?site_url('superadmin/save_kebun'):site_url('databaseKebun/save_kebun'); ?>">
			<?php
				foreach($form_kebun as $key=>$value){
					echo '<div class="form-group row">';
					echo print_form($value,true);
					echo '</div>';
				}					
			?>
				<div class="form-group">
					<input type=submit class="btn btn-primary" type="button" value="Simpan">
				</div>
			</form>
		</div>
		<div class="mT-30">
			<table class="table table-hover">
				<tr>
					<th><div style="text-align:center">No Kebun</div></th>
					<th><div style="text-align:center">Ukuran Kebun (Ha)</div></th>
					<th><div style="text-align:center">Koordinat GPS</div></th>
					<th><div style="text-align:center">Tersertifikasi</div></th>
					<th><div style="text-align:center">Aksi</div></th>
				</tr>
			<?php
				$i=1;
				foreach($list_kebun as $key=>$val){					
					echo '<tr>
						<td><div style="text-align:center">'.$i++.'</div></td>
						<td><div style="text-align:center">'.$val['kebunUkuran'].'</div></td>
						<td><div style="text-align:center">'.$val['kebunKoordinat'].'</div></td>
						<td><div style="text-align:center">'.$val['kebunSertifikasi'].'</div></td>
						<td>
							<button class="__delete-item btn btn-danger">Hapus</button>
							<div class="__delete-confirm" style="display:none;">
								'.'<a href="'.(($_SESSION['type']==7)?site_url('superadmin/delete_kebun/'.$val['kebunId']):site_url('databaseKebun/delete_kebun/'.$val['kebunId'])).'" class="btn btn-danger">Hapus</a>
								<button class="__cancel-delete btn btn-success">Cancel</button>
							</div> 
						</td>
					</tr>';
				}					
			?>
			</table>
		</div>
	</div>
</div>
