<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="masonry-sizer col-md-12"></div>

<div class="masonry-item col-md-12 mT-30 w-100">
	<div class="bgc-white p-20 bd">				
		<h3 class="c-grey-900"><?php echo $title; ?></h3>
				
		<div class="mT-30">
			<table class="table table-hover __dataTable__">
				<thead>
				<tr>
					<th><div style="text-align:center">No</div></th>
					<th><div style="text-align:center">Nama</div></th>
					<th><div style="text-align:center"></div></th>
					<th><div style="text-align:center">Alamat</div></th>
					<th><div style="text-align:center">Hasil</div></th>
					<th><div style="text-align:center"></div></th>
				</tr>
				</thead>
				<tbody>
			<?php
				$i=1;
				foreach($table as $key=>$val){
					echo '<tr>
						<td><div style="text-align:center">'.$i++.'</div></td>
						<td><div style="text-align:center">'.$val['profilNama'].'</div></td>
						<td>
						    <div style="text-align:center">'.$val['profTypeName'].'
						    </div>
						</td>
						<td>
						    <div style="text-align:center">
						        '.$val['profilDesa'].', '.$val['profilKecamatan'].', '.$val['profilKabupaten'].', '.$val['profilProvinsi'].'
						    </div>
						</td>
						<td>
						    <div style="text-align:center">
						    '.($val['hasil'] ? '<span style="color:green">LOLOS</span>' : '<span style="color:red">TIDAK LOLOS</span>').'
						    </div>
						</td>
						<td><div style="text-align:center" class="volumeTotal"><a href="'.site_url('sertifikasi/hasilSertifikasi/'.$val['profilUserId']).'">LIHAT HASIL</a></div></td>
					</tr>';
				}					
			?>
				</tbody>
			</table>
		</div>
	</div>
</div>
