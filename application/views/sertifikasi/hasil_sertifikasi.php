<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="masonry-sizer col-md-12"></div>
<div class="masonry-item col-md-12 mT-30">
	<div class="bgc-white p-20 bd">
		<h3 class="c-grey-900" style='float:left;'><?php echo $title; ?></h3>
		<button id='toPDF' class="btn btn-lg btn-success" style='float:right;margin-bottom: 10px;'>PDF</button>

		<?php
			if(isset($_SESSION['sertifikasi-report'])){
				$color['BAIK'] = 'success';
				$color['CUKUP'] = 'warning';
				$color['CRITICAL'] = 'danger';
		?>
				<h1>Resume Sertifikasi : <span class="badge badge-<?php echo $color[$_SESSION['sertifikasi-report']]; ?>"><?php echo $_SESSION['sertifikasi-report']; ?></span></h1>
		<?php
				unset($_SESSION['sertifikasi-report']);
			}
		?>

		<div class="mT-30">
			<form method=POST action='<?php echo site_url('sertifikasi/save_form') ?>'>
				<table class='table table-hover' id="hasilSertifikasi">
					<tr>
						<th>Nama Petani</th>
						<th colspan=3>
							<h3 id="idPetani"><?php echo $profile['profilUserId'].' - '.$profile['profilNama'] ;?></h3>
						</th>
            <th></th>
            <th></th>
					</tr>

					<tr>
						<th>Hasil Sertifikasi</th>
						<th colspan=3>
							<h3><?php echo $hasilSertifikasi;?></h3>
						</th>
					</tr>
					<?php
						$prinNum = 1;
						foreach($formSertifikasi as $principle){
							echo '<tr>
									<th colspan=4><h4>'.$principle['NAME'].'</h4></th>
								</tr>';

							$typeNum = 1;
							foreach($principle['TYPE'] as $type){
								echo '<tr>
										<td colspan=4><h5>'.$type['NAME'].'</h5></td>
									</tr>';

												//~ <select name="'.$question['usrsertSertId'].'" required>
													//~ <option value="" '.(($question['usrsertValue']==null)?('selected'):('')).'>-</option>
													//~ <option value="NC" '.(($question['usrsertValue']=='NC')?('selected'):('')).'>NC</option>
													//~ <option value="C" '.(($question['usrsertValue']=='C')?('selected'):('')).'>C</option>
													//~ <option value="NA" '.(($question['usrsertValue']=='NA')?('selected'):('')).'>NA</option>
												//~ </select>
								foreach($type['QUESTION'] as $sertId => $question){
									echo '<tr>
											<td>'.$prinNum.'.'.$typeNum.' </td>
											<td><b>'.$question['CLASS'].'</b></td>
											<td> '.$question['TEXT'].'</td>
											<td><b>'.$question['VALUE'].'</b><b> </b></td>
										</tr>';
									$typeNum++;
								}
							}
							$prinNum++;
						}
					?>
				</table>
			</form>
		</div>
	</div>
</div>

<script>

	function pdfForElement(id) {
		function ParseContainer(cnt, e, p, styles) {
			var elements = [];
			var children = e.childNodes;
			if (children.length != 0) {
				for (var i = 0; i < children.length; i++) p = ParseElement(elements, children[i], p, styles);
			}
			if (elements.length != 0) {
				for (var i = 0; i < elements.length; i++) cnt.push(elements[i]);
			}
			return p;
		}

		function ComputeStyle(o, styles) {
			for (var i = 0; i < styles.length; i++) {
				var st = styles[i].trim().toLowerCase().split(":");
				if (st.length == 2) {
					switch (st[0]) {
						case "font-size":
							{
								o.fontSize = parseInt(st[1]);
								break;
							}
						case "text-align":
							{
								switch (st[1]) {
									case "right":
										o.alignment = 'right';
										break;
									case "center":
										o.alignment = 'center';
										break;
								}
								break;
							}
						case "font-weight":
							{
								switch (st[1]) {
									case "bold":
										o.bold = true;
										break;
								}
								break;
							}
						case "text-decoration":
							{
								switch (st[1]) {
									case "underline":
										o.decoration = "underline";
										break;
								}
								break;
							}
						case "font-style":
							{
								switch (st[1]) {
									case "italic":
										o.italics = true;
										break;
								}
								break;
							}
					}
				}
			}
		}

		function ParseElement(cnt, e, p, styles) {
			if (!styles) styles = [];
			if (e.getAttribute) {
				var nodeStyle = e.getAttribute("style");
				if (nodeStyle) {
					var ns = nodeStyle.split(";");
					for (var k = 0; k < ns.length; k++) styles.push(ns[k]);
				}
			}

			switch (e.nodeName.toLowerCase()) {
				case "#text":
					{
						var t = {
							text: e.textContent.replace(/\n/g, "")
						};
						if (styles) ComputeStyle(t, styles);
						p.text.push(t);
						break;
					}
				case "h3":
				case "h4":
				case "h5":
				case "b":
				case "strong":
					{
						//styles.push("font-weight:bold");
						ParseContainer(cnt, e, p, styles.concat(["font-weight:bold"]));
						break;
					}
				case "u":
					{
						//styles.push("text-decoration:underline");
						ParseContainer(cnt, e, p, styles.concat(["text-decoration:underline"]));
						break;
					}
				case "i":
					{
						//styles.push("font-style:italic");
						ParseContainer(cnt, e, p, styles.concat(["font-style:italic"]));
						//styles.pop();
						break;
						//cnt.push({ text: e.innerText, bold: false });
					}
				case "span":
					{
						ParseContainer(cnt, e, p, styles);
						break;
					}
				case "br":
					{
						p = CreateParagraph();
						cnt.push(p);
						break;
					}
				case "table":
					{
						var t = {
							table: {
								widths: [],
								body: []
							}
						}
						var border = e.getAttribute("border");
						var isBorder = false;
						if (border)
							if (parseInt(border) == 1) isBorder = true;
						if (!isBorder) t.layout = 'noBorders';
						ParseContainer(t.table.body, e, p, styles);

						var widths = e.getAttribute("widths");
						if (!widths) {
							if (t.table.body.length != 0) {
								if (t.table.body[0].length != 0)
									for (var k = 0; k < t.table.body[0].length; k++) t.table.widths.push("*");
							}
						} else {
							var w = widths.split(",");
							for (var k = 0; k < w.length; k++) t.table.widths.push(w[k]);
						}
						cnt.push(t);
						break;
					}
				case "tbody":
					{
						ParseContainer(cnt, e, p, styles);
						//p = CreateParagraph();
						break;
					}
				case "tr":
					{
						var row = [];
						ParseContainer(row, e, p, styles);
						cnt.push(row);
						break;
					}
				case "th":
				case "td":
					{
						p = CreateParagraph();
						var st = {
							stack: []
						}
						st.stack.push(p);

						var rspan = e.getAttribute("rowspan");
						if (rspan) st.rowSpan = parseInt(rspan);
						var cspan = e.getAttribute("colspan");
						if (cspan) st.colSpan = parseInt(cspan);

						ParseContainer(st.stack, e, p, styles);
						cnt.push(st);
						break;
					}
				case "div":
				case "p":
					{
						p = CreateParagraph();
						var st = {
							stack: []
						}
						st.stack.push(p);
						ComputeStyle(st, styles);
						ParseContainer(st.stack, e, p);

						cnt.push(st);
						break;
					}
				default:
					{
						console.log("Parsing for node " + e.nodeName + " not found");
						break;
					}
			}
			return p;
		}

		function ParseHtml(cnt, htmlText) {
			var html = $(htmlText.replace(/\t/g, "").replace(/\n/g, ""));
			var p = CreateParagraph();
			for (var i = 0; i < html.length; i++) ParseElement(cnt, html.get(i), p);
		}

		function CreateParagraph() {
			var p = {
				text: []
			};
			return p;
		}
		content = [];
		ParseHtml(content, document.getElementById(id).outerHTML);
		return pdfMake.createPdf({
			content: content
		});
	}

	function convert (id,filename = 'file') {
		//~ var html = $('#'+id).html();
		//~ var val = htmlToPdfmake(html);
		//~ var dd = {content:val};
		//~ pdfMake.createPdf(dd).download(filename);

		pdfForElement(id).download(filename);
	}

	$(document).ready(function(){
		$('#toPDF').click(function(){
			const filename = $('#idPetani').text();
			convert('hasilSertifikasi','[SERTIFIKASI] '+filename+'.pdf');
		});
	});
</script>
