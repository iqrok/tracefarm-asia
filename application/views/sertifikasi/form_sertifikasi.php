<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="masonry-sizer col-md-12"></div>
<div class="masonry-item col-md-12 mT-30">
	<div class="bgc-white p-20 bd">
		<h3 class="c-grey-900"><?php echo $title; ?></h3>

		<?php
			if(isset($_SESSION['sertifikasi-report'])){
				$color['BAIK'] = 'success';
				$color['CUKUP'] = 'warning';
				$color['CRITICAL'] = 'danger';
		?>
				<h1>Resume Sertifikasi : <span class="badge badge-<?php echo $color[$_SESSION['sertifikasi-report']]; ?>"><?php echo $_SESSION['sertifikasi-report']; ?></span></h1>
		<?php
				unset($_SESSION['sertifikasi-report']);
			}
		?>
		
		<div class="mT-30">
			<form method=POST action='<?php echo site_url('sertifikasi/save_form') ?>'>
				<table class='table table-hover'>
					<tr>
						<th>Nama Petani</th>
						<th colspan=2>							
							<select class="form-control" name="userId">
					<?php
						foreach($listUser as $petani){
							echo '<option value="'.$petani['key'].'">'.$petani['val'].'</option>';
						}
					?>
							</select>
						</th>
					</tr>
					
					<?php	
						$prinNum = 1;
						foreach($formSertifikasi as $principle){
							echo '<tr>
									<th colspan=4><h4>'.$principle['NAME'].'</h4></th>
								</tr>';
					
							$typeNum = 1;
							foreach($principle['TYPE'] as $type){
								echo '<tr>
										<td colspan=4><h5>'.$type['NAME'].'</h5></td>
									</tr>';
											
												//~ <select name="'.$question['usrsertSertId'].'" required>
													//~ <option value="" '.(($question['usrsertValue']==null)?('selected'):('')).'>-</option>
													//~ <option value="NC" '.(($question['usrsertValue']=='NC')?('selected'):('')).'>NC</option>
													//~ <option value="C" '.(($question['usrsertValue']=='C')?('selected'):('')).'>C</option>
													//~ <option value="NA" '.(($question['usrsertValue']=='NA')?('selected'):('')).'>NA</option>
												//~ </select>
								foreach($type['QUESTION'] as $sertId => $question){
									echo '<tr>
											<td>'.$prinNum.'.'.$typeNum.'</td>
											<td>'.$question['CLASS'].'</td>
											<td>'.$question['TEXT'].'</td>
											<td>
												<select name="'.$sertId.'" required>
													<option value="" >-</option>
													<option value="NC" >NC</option>
													<option value="C" >C</option>
													<option value="NA" >NA</option>
												</select>
											</td>
										</tr>';
									$typeNum++;
								}
							}
							$prinNum++;
						}
					?>
					<tr>
						<td colspan=4>
							<input type=submit class="btn btn-block btn-primary" type="button" value="SIMPAN">
						</td>
					</tr>
				</table>
			</form>
		</div>
	</div>
</div>
