<?php 
	defined('BASEPATH') OR exit('No direct script access allowed');
?>
<script>
	function checkType(){
		var type = $('#profil_jenis').val();
		var elems = [
						$('#profil_JmlKel'),
						$('#profil_sertifikat'),
						$('#profil_organik'),
						$('#profil_Kelompok'),
						$('#profil_kebuUkuran'),
						$('#profil_thnDepan'),
						$('#profil_thnlalu'),
						$('#profil_kebuNr'),
					];
					
		if(type==2){
			for(var key in elems){
				elems[key].parent().parent().show('fast');
			}
		}
		else{
			for(var key in elems){
				elems[key].parent().parent().hide('fast');
			}
		}
	}
	
	$(document).ready(function(){
		checkType();
	});
	
	$(document).ready(function(){
		$('#profil_Provinsi').change(function(){
			let kabupaten = $('#profil_Kabupaten');
			let province = $(this).val();
			let url = "<?php echo site_url('profile/getRegencies/');?>";
			kabupaten.html('');
			$.post( url,{province:province}, function( response ) {
				response = JSON.parse(response);
				for(let key in response){
					kabupaten.append('<option value=\''+response[key]+'\' data-id=\''+key+'\'>'+response[key]+'</option>')
				}
			});
		});
	});
	
	$(document).ready(function(){
		$('#profil_Kabupaten').change(function(){
			let kabupaten = $('#profil_Kabupaten').val();
			let kecamatan = $('#profil_Kecamatan');
			let province = $('#profil_Provinsi').val();
			let url = "<?php echo site_url('profile/getDistricts/');?>";
			kecamatan.html('');
			$.post( url,{province:province,kabupaten:kabupaten}, function( response ) {
				//~ console.log(response);
				response = JSON.parse(response);
				for(let key in response){
					kecamatan.append('<option value=\''+response[key]+'\' data-id=\''+key+'\'>'+response[key]+'</option>')
				}
			});
		});
	});
	
	$(document).ready(function(){
		$('#profil_Kecamatan').change(function(){
			let kabupaten = $('#profil_Kabupaten').val();
			let kecamatan = $('#profil_Kecamatan').val();
			let kelurahan = $('#profil_Kelurahan');
			let province = $('#profil_Provinsi').val();
			let url = "<?php echo site_url('profile/getVillages/');?>";
			kelurahan.html('');
			$.post( url,{province:province,kabupaten:kabupaten,kecamatan:kecamatan}, function( response ) {
				//~ console.log(response);
				response = JSON.parse(response);
				for(let key in response){
					kelurahan.append('<option value=\''+response[key]+'\' data-id=\''+key+'\'>'+response[key]+'</option>')
				}
			});
		});
	});
	
	$(document).ready(function(){
		$('#profil_jenis').change(function(){
			checkType();
		});
	});
	
</script>
