<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<table class="table table-hover">
	<tr>
		<th>No</th>
		<th>Penerima</th>
		<th>Tanggal</th>
		<th>Volume (Ton)</th>
		<th>Aksi</th>
	</tr>
<?php
	$i=1;
	foreach($log as $key=>$val){					
		echo '<tr>
			<td>'.$i++.'</td>
			<td>'.$val['profilNama'].'</td>
			<td>'.convert_date($val['logTimestamp'],true).'</td>
			<td>'.$val['logVolume'].'</td>
			<td>
				<button class="__delete-item btn btn-danger">Hapus</button>
				<div class="__delete-confirm" style="display:none;">
					<a href="'.site_url('profile/delete_log/'.$val['logId']).'" class="btn btn-danger">Hapus</a>
					<button class="__cancel-delete btn btn-success">Cancel</button>
				</div> 
			</td>
		</tr>';
	}					
?>
</table>
