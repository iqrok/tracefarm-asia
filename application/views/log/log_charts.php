<?php defined('BASEPATH') OR exit('No direct script access allowed');?>

<?php
	foreach($container as $key=>$val){?>		
		<div class="masonry-item col-md-6 mT-30">
			<div class="bgc-white p-20 bd">
				<h3 class="c-grey-900" style="text-align:center;"><?php echo $box_title[$key]; ?></h3>
				<div class="mT-30 ">
					<?php echo $val; ?>
				</div>
			</div>
		</div>

<?php
	}
	echo $script;
?>
