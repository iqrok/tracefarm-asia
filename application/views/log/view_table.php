<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php if(isset($_SESSION['type']) && $_SESSION['type']!=null): ?>
<div class="masonry-item col-md-12 mT-30">
	<div class="bgc-white p-20 bd">
		<h3 class="c-grey-900"><?php echo $title_log; ?></h3>
		<div class="layers bd bgc-white p-20">
			<div class="layer w-100">
				<div class="peers ai-sb fxw-nw">
					<div class="peer peer-greed">
						<h4 class="lh-1">Tambah Log</h4>
					</div>
					<div class="peer">
						<a href="<?php echo site_url('log');?>" class="btn btn-success">Tambah</a>
					</div>
				</div>
			</div>
		</div>
		
		<div class="layers bd bgc-white p-20  mT-30">
			<form class="layer w-100">
				<div class="row">
					<div class="col-md-3">
						<h4 class="lh-1">Bulan-Tahun</h4>
					</div>
					<div class="col-md-6">
						<input class="datepicker_month form-control" name="range" placeholder="MM-YYYY">
					</div>
					<div class="col-md-3">
						<input type="submit" class="btn btn-primary" value="Pilih Range">
					</div>
				</div>
			</form>
		</div>
		
		<div class="">
			<?php echo $log_table; ?>
		</div>
	</div>
</div>
<?php endif; ?>
