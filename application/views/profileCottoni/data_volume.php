<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="masonry-sizer col-md-12"></div>

<div class="masonry-item col-md-12 mT-30 w-100">
	<div class="bgc-white p-20 bd">
		<h3 class="c-grey-900"><?php echo $title; ?></h3>
		<div class="mT-30">
			<form method=POST action="<?php echo site_url('databaseCottoni/save_volume'); ?>">
			<?php
				foreach($form as $key=>$value){
					echo '<div class="form-group row">';
					echo print_form($value,true);
					echo '</div>';
				}
			?>
				<div class="form-group">
					<input type=submit class="btn btn-primary" type="button" value="Simpan">
				</div>
			</form>
		</div>
		<div class="mT-30">
			<table class="table table-hover">
				<tr>
					<th><div style="text-align:center">No</div></th>
					<th><div style="text-align:center">Tanggal Pencatatan</div></th>
					<th><div style="text-align:center">Kotoran (%)</div></th>
					<th><div style="text-align:center">Kadar Air (%)</div></th>
					<th><div style="text-align:center">Warna</div></th>
					<th><div style="text-align:center">Volume (ton)</div></th>
				</tr>
			<?php
				$i=1;
				foreach($list as $key=>$val){
					echo '<tr>
						<td><div style="text-align:center">'.$i++.'</div></td>
						<td><div style="text-align:center">'.convert_date($val['volumeTimestamp']).'</div></td>
						<td><div style="text-align:center">'.$val['volumeWaste'].'%</div></td>
						<td><div style="text-align:center">'.$val['volumeKadarAir'].'%</div></td>
						<td><div style="text-align:center"><b>'.$val['volumeBeanCount'].'</b></div></td>
						<td><div style="text-align:center">'.$val['volumeWeight'].' ton</div></td>
					</tr>';
				}
			?>
			</table>
		</div>
	</div>
</div>
