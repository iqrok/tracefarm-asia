<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="masonry-sizer col-md-6"></div>

<div class="masonry-item col-md-12 w-100">
<div class="masonry-item col-md-6">
    <div class="bd bgc-white">
        <div class="layers">
            <div class="layer w-100">
                <div class="bgc-light-blue-500 c-white p-20">
                    <div class="peers ai-c jc-sb gap-40">
                        <div class="peer peer-greed">
                            <h5>Artikel</h5>
                            <p class="mB-0">Kumpulan Informasi</p>
                        </div>
                    </div>
                </div>
                <div class="table-responsive p-20">
                    <table class="table">
                        <thead>
                            <tr>
                                <th class="bdwT-0">Name</th>
                                <th class="bdwT-0">Action</th>
                            </tr>
                        </thead>
                        <tbody>
							<?php foreach($files['doc'] as $key=>$val){ ?>
                                <tr>
									<td><span class="text-success"><?php echo $val['fileTitle']; ?></span></td>
									<td><a class="btn btn-success" href='<?php echo $val['fileURL']; ?>' target=_blank>Baca</a></td>
								</tr>
							<?php } ?>
                            <tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="masonry-item col-md-6">
    <div class="bd bgc-white">
        <div class="layers">
            <div class="layer w-100">
                <div class="bgc-green-500 c-white p-20">
                    <div class="peers ai-c jc-sb gap-40">
                        <div class="peer peer-greed">
                            <h5>Video</h5>
                            <p class="mB-0">Kumpulan Video</p>
                        </div>
                    </div>
                </div>
                <div class="table-responsive p-20">
                    <table class="table">
                        <thead>
                            <tr>
                                <th class="bdwT-0"></th>
                                <th class="bdwT-0"></th>
                            </tr>
                        </thead>
                        <tbody>
							<?php foreach($files['video'] as $key=>$val){ ?>
                                <tr>
									<td><?php echo $val['iframe']; ?></td>
									<td><h4><?php echo $val['fileTitle']; ?></h4></td>
								</tr>
							<?php } ?>
                            <tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
