<?php defined('BASEPATH') OR exit('No direct script access allowed');?>

<div class="masonry-sizer col-md-6"></div>
<div class="masonry-item w-100 mT-30">
	<div class="bgc-white p-20 bd">
		<h3 class="c-grey-900"><?php echo $price['title']; ?></h3>
		<div class="mT-30">
			<?php echo $price['table']; ?>
			<div class="alert alert-light" role="alert">Source : <a href='https://www.icco.org/statistics/cocoa-prices/daily-prices.html' target=_blank>icco.org</a></div>
		</div>
	</div>
</div>
