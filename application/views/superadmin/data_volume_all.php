<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="masonry-sizer col-md-12"></div>

<div class="masonry-item col-md-12 mT-30 w-100">
	<div class="bgc-white p-20 bd">				
		<h3 class="c-grey-900"><?php echo $title; ?></h3>
				
		<div class="mT-30">
			<table class="table table-hover __dataTable__">
				<thead>
					<tr>
						<th><div style="text-align:center">No</div></th>
						<th><div style="text-align:center">Nama</div></th>
						<th><div style="text-align:center"></div></th>
						<th><div style="text-align:center">Tanggal Pencatatan</div></th>
						<th><div style="text-align:center">Waste (%)</div></th>
						<th><div style="text-align:center">Kadar Air (%)</div></th>
						<th><div style="text-align:center">Bean Count</div></th>
						<th><div style="text-align:center">Volume (ton)</div></th>
						<th><div style="text-align:center">Total Volume (ton)</div></th>
					</tr>
				</thead>
				<tbody>
				<?php
					$i=1;
					foreach($table as $key=>$val){
						echo '<tr>
							<td><div style="text-align:center">'.$i++.'</div></td>
							<td><div style="text-align:center">'.$val['profilNama'].'</div></td>
							<td><div style="text-align:center">'.$val['profTypeName'].'</div></td>
							<td><div style="text-align:center">'.convert_date($val['volumeTimestamp']).'</div></td>
							<td><div style="text-align:center">'.$val['volumeWaste'].'%</div></td>
							<td><div style="text-align:center">'.$val['volumeKadarAir'].'%</div></td>
							<td><div style="text-align:center">Grade <b>'.$val['volumeBeanCount'].'</b></div></td>
							<td><div style="text-align:center" class="volumeNow" data-volume="'.$val['volumeWeight'].'">'.$val['volumeWeight'].' ton</div></td>
							<td><div style="text-align:center" class="volumeTotal">'.$val['totalVolume'].' ton</div></td>
						</tr>';
					}					
				?>
				</tbody>
			</table>
		</div>
	</div>
</div>
