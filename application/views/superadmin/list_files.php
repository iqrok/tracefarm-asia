<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="masonry-sizer col-md-12"></div>

<div class="masonry-item col-md-12 mT-30 w-100">
	<div class="bgc-white p-20 bd">				
		<h3 class="c-grey-900" style="float:left;">List Files</h3>
		<a class='btn btn-success' style="float:right;" href='<?php echo site_url('superadmin/insertUpdateNewFile'); ?>'>Add New</a>
		<div class="mT-30">
			<table class="table table-hover __dataTable__">
				<thead>
				<tr>
					<th><div style="text-align:center">No</div></th>
					<th><div style="text-align:center">Tipe File</div></th>
					<th><div style="text-align:center">Judul</div></th>
					<th><div style="text-align:center">URL</div></th>
					<th><div style="text-align:center">Action</div></th>
				</tr>
				</thead>
				<tbody>
			<?php
				$i=1;
				foreach($list as $key=>$val){					
					echo '<tr>
						<td><div style="text-align:center">'.$i++.'</div></td>
						<td><div style="text-align:center">'.$val['fileType'].'</div></td>
						<td><div style="text-align:center">'.$val['fileTitle'].'</div></td>
						<td><div style="text-align:center">'.$val['fileURL'].'</div></td>
						<td>
							<button class="__delete-item btn btn-danger">Hapus</button>
							<div class="__delete-confirm" style="display:none;">
								'.'<a href="'.(($_SESSION['type']==7)?site_url('superadmin/delete_file/'.$val['fileId']):'').'" class="btn btn-danger">Hapus</a>
								<button class="__cancel-delete btn btn-success">Cancel</button>
							</div>
						</td>
					</tr>';
				}					
			?>
				</tbody>
			</table>
		</div>
	</div>
</div>
