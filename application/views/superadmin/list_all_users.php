<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="masonry-sizer col-md-12"></div>

<div class="masonry-item col-md-12 mT-30 w-100">
	<div class="bgc-white p-20 bd">				
		<h3 class="c-grey-900"><?php echo (isset($title)?$title:"List Penggguna"); ?></h3>
		
		<div class="mT-30">
			<table class="table table-hover __dataTable__">
				<thead>
					<tr>
						<th><div style="text-align:center">No</div></th>
						<th><div style="text-align:center">ID</div></th>
						<th><div style="text-align:center">Nama</div></th>
						<th><div style="text-align:center">Jenis</div></th>
						<th><div style="text-align:center">Action</div></th>
					</tr>
				</thead>
				<tbody>
				<?php
					$i=1;
					foreach($list as $key=>$val){					
						echo '<tr>
							<td><div style="text-align:center">'.$i++.'</div></td>
							<td><div style="text-align:center">'.$val['profilUserId'].'</div></td>
							<td><div style="text-align:center">'.$val['profilNama'].'</div></td>
							<td><div style="text-align:center">'.$val['profTypeName'].'</div></td>
							<td>
								<div style="text-align:center">
									<a class="btn btn-primary" href="'.site_url('superadmin/editUser/'.$val['profilUserId'].'/'.$val['profTypeId']).'">Edit</a>
									'.(($val['profTypeId']==2)?('<a class="btn btn-success" href="'.site_url('superadmin/editKebun/'.$val['profilUserId']).'">Edit Kebun</a>'):('')).'
								</div>
							</td>
						</tr>';
					}					
				?>
				</tbody>
			</table>
		</div>
	</div>
</div>
