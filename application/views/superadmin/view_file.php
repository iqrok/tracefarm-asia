<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="masonry-sizer col-md-6"></div>
<div class="masonry-item w-100 mT-30">
	<div class="bgc-white p-20 bd">
		
		<?php if(isset($_SESSION['alert'])): ?>
		<div class="alert alert-info" role="alert"><?php echo $_SESSION['alert']; ?></div>
		<?php endif;?>
		
		<h3 class="c-grey-900"><?php echo $title; ?></h3>
		<div class="mT-30">					
			<form method=POST action="<?php echo ($_SESSION['type']==7)?site_url('superadmin/save_file'):(''); ?>">
			<?php
				foreach($form as $key=>$value){
					echo '<div class="form-group row">';
					echo print_form($value,true);
					echo '</div>';
				}					
			?>
				<div class="form-group">
					<input type=submit class="btn btn-primary" type="button" value="Simpan">
				</div>
			</form>
		</div>
	</div>
</div>
