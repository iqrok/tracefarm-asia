<?php
	defined('BASEPATH') OR exit('No direct script access allowed');				
	
	if(!function_exists('convert_date')){			
		function convert_date($date,$useDayName=0){
			// date format is YYYY-MM-DD
			
			$day = array("Minggu","Senin","Selasa","Rabu","Kamis","Jumat","Sabtu");
			$month = array("","Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember");
			
			$dayName = "";
			
			if(is_numeric($date)){
				//if it's unix timestamp, convert it to YYYY-MM-DD
				if($date<=0)
					return; 
					
				if($useDayName)
					$dayName = $day[(int)date("w",$date)].", ";
					
				$date = date("Y-m-d",$date);
			}
			
			if($date == "0000-00-00")
				return "-";
			
			$date = explode("-",$date);
			
			$dateName = (int)$date[2];		
			$monthName = $month[(int)$date[1]];
			$yearName = $date[0];
						
			return $dayName.$dateName." ".$monthName." ".$yearName;
		}
	}
	
	if(!function_exists('print_form')){
		function print_form($identity,$inline=FALSE,$col = array(3,9)){
			$col_left = $col[0];
			$col_right = $col[1];

			if($identity['type']!='data')
				if($inline === FALSE)
					echo form_label($identity['label'],$identity['attr']['id'],array('class'=>'text-normal text-dark'));
				else{
					echo form_label($identity['label'],$identity['attr']['id'],array('class'=>'text-normal text-dark col-md-'.$col_left));
				}
			
			switch($identity['type']){
				case 'input':
					if($inline === FALSE)
						echo form_input($identity['attr']);
					else
						echo "<div class='col-md-".$col_right."'>".form_input($identity['attr'])."</div>";
					break;
					
				case 'password':
					if($inline === FALSE)
						echo form_password($identity['attr']);
					else
						echo "<div class='col-md-".$col_right."'>".form_password($identity['attr'])."</div>";
					break;
					
				case 'select':
					if($inline === FALSE)
						echo form_dropdown($identity['attr']['name'], $identity['options'], $identity['selected'], $identity['attr']);
					else
						echo "<div class='col-md-".$col_right."'>".form_dropdown($identity['attr']['name'], $identity['options'], $identity['selected'], $identity['attr'])."</div>";
					break;
				
				case 'label':
					if($inline === FALSE)
						echo "<p class='form-control-static'>".$identity['text']."</p>";
					else
						echo "<div class='col-md-".$col_right."'><p class='form-control-static'>".$identity['text']."</p></div>";
					break;

				case 'data':
					break;
					
				default: break;
			}			
		}
	}
	
	if(!function_exists('p_form_group')){
		function p_form_group($identity,$inline=FALSE,$col = array(3,9)){
			echo "<div class='from-group'>";
			echo print_form($identity,$inline,$col);
			echo "</div>";
		}
	}
	
	if(!function_exists('update_value_array')){
		function update_value_array(&$array, $path, $value=NULL){
			// got from here :
			// https://stackoverflow.com/questions/27929875/how-to-access-and-manipulate-multi-dimensional-array-by-key-names-path
			//
			
			$path = explode('->', $path);
			$temp = &$array;

			foreach($path as $key) {
				$temp = &$temp[$key];
			}
			$temp = $value;
		}
	}
	
	if(!function_exists('get_value_array')){
		function get_value_array($array, $path) {
			// got from here :
			// https://stackoverflow.com/questions/27929875/how-to-access-and-manipulate-multi-dimensional-array-by-key-names-path
			//
			
			$path = explode('->', $path);
			$temp =& $array;

			foreach($path as $key) {
				$temp =& $temp[$key];
			}
			return $temp;
		}
	}
	
	if(!function_exists('unset_array_index')){
		function unset_array_index(&$array,$path) {
			// got from here :
			// https://stackoverflow.com/questions/27929875/how-to-access-and-manipulate-multi-dimensional-array-by-key-names-path
			//
			
			$path = explode('->', $path);
			$temp =& $array;
			
			$depth = sizeof($path)-1;
			$cnt=0;
			foreach($path as $key) {
				if($cnt++>=$depth) {
					unset($temp[$key]);
				} else {
					$temp =& $temp[$key];
				}
			}
		}
	}
	
	if(!function_exists('toRoman')){
		function toRoman($N){
			$c='IVXLCDM';
			for($a=5,$b=$s='';$N;$b++,$a^=7)
					for($o=$N%$a,$N=$N/$a^0;$o--;$s=$c[$o>2?$b+$N-($N&=-2)+$o=1:$b].$s);
			return $s;
		}	
	}
	
	if(!function_exists('is_multidimensional_array')){
		function is_multidimensional_array($array,$return = 'BOOLEAN'){
			if($return == 'BOOLEAN')
				return (count($array,COUNT_RECURSIVE) == count($array))?(false):(true);
			else if($return == 'STRING')
				return (count($array,COUNT_RECURSIVE) == count($array))?('false'):('true');
			else if($return == 'INTEGER')
				return (count($array,COUNT_RECURSIVE) == count($array))?(0):(1);
		}	
	}
	
	if(!function_exists('__bulan')){
		function __bulan($x){
			global $bulan;
			return ($x>0 && $x<=12)?($bulan[$x]):(NULL);
		}	
	}
	
	if(!function_exists('__hari')){
		function __hari($x,$z=false){
			if($x<0 && $x>7)
				return NULL;
				
			$hari0 = array('Minggu','Senin','Selasa','Rabu','Kamis','Jumat','Sabtu');
			$hari1 = array('','Senin','Selasa','Rabu','Kamis','Jumat','Sabtu','Minggu');
			return ($z)?($hari1[$x]):($hari0[$x]);
		}	
	}
	
	if(!function_exists('console_html')){
		function console_html($x){
			echo "<pre>";
			var_dump($x);
			echo"</pre>";
		}	
	}
	
	if ( ! function_exists( 'convert_table2array' ) ){
		function convert_table2array($html,$isAssoc = true){
		    /** /
			$DOM = new DOMDocument;
			$DOM->loadHTML($html);

			$table['header'] = $DOM->getElementsByTagName('th');
			$table['details'] = $DOM->getElementsByTagName('td');

			if($table['header']['length'] == 0){
				$isAssoc = false;
				$simpleHTML = str_get_html($html);
				$simpleTable = $simpleHTML->find('table',0);

				$num_of_cols = 0;
				foreach($simpleTable->find('tr') as $tr)
				{
					$c=0;				
					foreach($tr->find('td') as $td)
						$c++;
						
					if( $c>$num_of_cols || !isset($num_of_cols) )
						$num_of_cols = $c; 
				}
			}
			
			//create array from th tag as key for assoc array
			foreach($table['header'] as $header){
				$key[] = trim($header->textContent);
			}

			//create array from td tag
			$i = 0;
			$j = 0;
			$divider = (isset($key))?(count($key)):($num_of_cols);
			foreach($table['details'] as $details){
				$content[$j][] = trim($details->textContent);
				$i = $i + 1;
				$j = $i % $divider == 0 ? $j + 1 : $j;
			}

			if(!$isAssoc)
				return $content;

			//convert into associative array
			for($i = 0; $i < count($content); $i++){
				for($j = 0; $j < $divider; $j++){
					$tmp[$i][$key[$j]] = $content[$i][$j];
				}
			}
			$content = $tmp;
			unset($tmp);

			return $content;
			/**/
		}
	}
