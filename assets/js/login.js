var slideShow;
const __INTERVAL = 15111;
function toggleLoginImg(){
	let loginImg = $('#loginImg').children();
	let currentImg = null;
	$.each(loginImg,function(index,elem){
		let attrs = $(this).attr('class');
		//~ console.log(attrs);
		if(!(attrs && attrs.match(/imgHide/g))){
			currentImg=index;
		}
	});
	let nextImg = (currentImg+1>=loginImg.length)?(0):(currentImg+1);
	let nextLoginImg = loginImg.eq(nextImg);
	nextLoginImg.siblings().hide().attr('class','imgHide');
	nextLoginImg.fadeIn(1000).attr('class','');

	clearInterval(slideShow);
	slideShow = setInterval(toggleLoginImg,__INTERVAL);
	return nextImg;
}

$(document).ready(function(){
	slideShow = setInterval(toggleLoginImg,__INTERVAL);
});

$(document).ready(function(){
	$('#loginImg').click(function(){
		toggleLoginImg();
	});
});
